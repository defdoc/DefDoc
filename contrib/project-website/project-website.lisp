(asdf:operate 'asdf:load-op :defdoc)

(defpackage :defdoc.contrib.project-website
    (:use :defdoc :defdoc.elements :defdoc.layout :common-lisp)
  (:export #:project-website
           #:name
           #:short-description
           #:documentation
           #:code
           #:communication
           #:dependencies))

(in-package :defdoc.contrib.project-website)

(defclass project-website (document)
  ((title :accessor name :initarg name :initform (error "Name is required."))
   (subtitle :initarg short-description)))

(defclass documentation (section)
  ((title :allocation :class :initform "Documentation")))

(defclass code (section)
  ((title :allocation :class :initform "Code")))

(defclass communication (section)
  ((title :allocation :class :initform "Communication")))

(defclass dependencies (section)
  ((title :allocation :class :initform "Dependencies")))

(defmethod convert ((input dependencies) (output t))
  (break)
  (convert (make-instance 'itemized-list (contents input)) output))

(defgeneric section-before-p (o1 o2))

#.`(progn
    ,@
    (let ((order '(documentation code dependencies communication)))
      (loop for (prior . following-list) on order
            nconc (loop for following in following-list
                        collect `(defmethod section-before-p ((o1 ,prior) (o2 ,following))
                                  t)
                        collect `(defmethod section-before-p ((o2 ,following) (o1 ,prior))
                                  nil)))))


(defmethod prepare :before ((input project-website) output)
  (setf (contents input) (sort (contents input) 'section-before-p)))
