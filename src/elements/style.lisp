(in-package :defdoc.elements)

(defclass bold (texual-structure-element)
  ()
  (:documentation "The contents of this container are to use a bolder font (if possible)."))

(defclass italic (texual-structure-element)
  ()
  (:documentation "The contents of this container are to toggle the use of italicization
relative to its parent."))

(defclass small-caps (texual-structure-element)
  ()
  (:documentation "The contents of this container are ideally to render
with lowercase letters using uppercase glpyhs scaled to be as tall as
the x-height, while preserving weighting."))
