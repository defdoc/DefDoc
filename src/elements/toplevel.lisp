(in-package :defdoc.elements)

(defclass toplevel-element (vertical-container wrapping-container-mixin)
  ((title :initform nil
          :initarg title
          :accessor title)
   (subtitle :initform nil
             :initarg subtitle
             :accessor subtitle)
   (author :initform nil
           :initarg author
           :accessor author)
   (date :initform nil
         :initarg date
         :accessor date)
   (copyright :initform nil
              :initarg copyright
              :accessor copyright)))

(defclass toplevel-subelement (vertical-container)
  ((toplevel-element :initarg toplevel-element)))

(defclass document (toplevel-element)
  ((sub-container-class :initform 'page)
   (title-page-p :type boolean
                 :initform nil
                 :initarg title-page-p
                 :accessor title-page-p
                 :documentation "Whether or not to create a title page if this document is
broken into multiple pages.")))

(defclass page (toplevel-subelement)
  ((page-number :type integer
                :initarg page-number
                :accessor page-number)))

(defclass report (document)
  ((title-page-p :initform t)))

(defclass book (document)
  ((title-page-p :initform t)))

(defclass article (document)
  ())

(defclass presentation (toplevel-element)
  ((sub-container-class :initform 'slide)
   (title-slide-p :type boolean
                  :initform t
                  :initarg title-slide-p
                  :accessor title-slide-p)))

(defclass slide (toplevel-subelement)
  ((slide-number :type integer
                 :initarg slide-number
                 :accessor slide-number)))
