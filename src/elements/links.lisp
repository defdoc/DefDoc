(in-package :defdoc.elements)

(defclass link (texual-structure-element)
  ((url :initarg url :type string :accessor url)))

(defclass citation (texual-structure-element)
  ((reference :initarg reference :accessor reference)
   (location :initarg location :accessor location)))
