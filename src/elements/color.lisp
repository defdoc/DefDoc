(in-package :defdoc)

(defclass named-color-spec ()
  ((name :type string
         :initarg :name
         :accessor name)))

(defclass rgb-color-spec ()
  ((red :type (real 0 1)
        :initarg :red
        :accessor red)
   (green :type (real 0 1)
          :initarg :green
          :accessor green)
   (blue :type (real 0 1)
         :initarg :blue
         :accessor blue)))

(defclass cmyk-color-spec ()
  ((cyan :type (real 0 1)
         :initarg :cyan
         :accessor cyan)
   (magenta :type (real 0 1)
            :initarg :magenta
            :accessor magenta)
   (yellow :type (real 0 1)
           :initarg :yellow
           :accessor yellow)
   (black :type (real 0 1)
          :initarg :black
          :accessor black)))

(defclass gray-color-spec ()
  ((value :type (real 0 1)
          :initarg :value
          :accessor value)))
