(in-package :defdoc)

#|
Fonts are specified by a font object, a symbol, or a numeric identifier. Numeric
identifiers are bound in the scope of document elements. The following numeric identifiers
are handled internally by DefDoc:

0 - the current base font
1 - the italicized or slanted version of the base font
2 - the bold version of the base font
3 - the bold italic version of the base font

|#

(defclass concrete-font ()
  ((family :type string
           :initarg family
           :accessor family)))

(defclass site-font ()
  ((name :type symbol
         :initarg name
         :accessor local-name))
  (:documentation "A site-local font, identified by the name used on the local site to
refer to it. The name is interned in the DEFDOC.SITE-FONTS package."))

(defpackage :defdoc.site-fonts)

(defmethod initialize-initialize :around ((object site-font) &key (('name name) nil)
                                          &rest initargs)
  (when name
    (let ((sym (intern name (find-package :defdoc.site-fonts))))
      (setf (slot-value object 'name) sym
            (symbol-value sym) object)))
  (loop while (remf initargs 'name))
  (multiple-value-prog1
      (apply #'call-next-method object slots initargs)
    (when (not (slot-boundp object 'name))
      (error "A SITE-FONT must be given a name."))))

;;;
;;; Visual Characteristics of Fonts
;;;

#|
Other parameters which may be added later:

Stroke variation - rapidness of change in the width of strokes curving through different angles
Transition angle - the angle at which the narrowest point in strokes occur
Arm style - convexness/concavity of the straight strokes and types of terminations on the C

|#

(defclass font-characteristics ()
  ((slant :type (or null (integer -90 90))
          :documentation "Place a line perpendicular to the baseline and intersect it with
a line parallel to the domainant stroke angle (often determined from the `H') the angle
from the vertical line counterclockwise to the stroke angle is the slant. Upright fonts
have a slany of 0; Italic or oblique fonts usually have a slant between -9 and -16.")
   (weight :type (or null (integer 0 10))
           :documentation "(* 10 (- 10 (log (/ stem-width cap-height) 1.5))) The point on
the vertical stroke halfway between the top and middle horizontal strokes of the letter
`E' is a good sample for measuring the stem width. A normal weight is 53, a typical bold
is 65.")
   (x-height :type (or null (integer 0 100))
             :documentation "(* 100 (/ x-tall cap-height)) This is the percentage
of the cap-height that the main portion of the lowercase letters occupy. Be sure to
measure parallel to the slant of the font.")
   (contrast :type (or null (integer 0 100))
             :documentation "100 minus 100 times the ratio of the thinnest to the thickest
parts of the stroke of the `O'. A font with minimal contrast has a value under 20, while a
font like Times New Roman has a contrast of 70. A high-contrast font like Poster Bodoni
has a contrast of 90.")
   (width :type (or null (integer 0 100))
          :documentation "(* 90 (+ 1.2 (log (log (+ 1 (/ O-width O-height)))))) If any
value is less than 0 or greater than 100, clamp it to that range. Times New Roman and
Helvetica both have a width of 70. A width of 100 is often present in Black/Wide Extended
fonts. Helvetica Condensed has a width of 55.")
   (width-variation :type (integer 0 100)
                    :documentation "(* 125 (- M-width J-width .2)) Measure the width of
the `M' at the midpoint of the height and the width of the `J' from the left edge of the
bowl or tail to the right edge of the main stroke horizontally and compensating for the
slant of the font. An old-style font will usually have a width-variation below
20. Monospaced fonts typically have a width-variation above 70.")
   (lowercase-style :type (member :normal :capital :small-caps :other)
                    :documentation "A symbol identifying what the lowercase letters are:
:NONE no lowercase letters are in this font
:NORMAL normal lowercase letters
:CAPITAL lowercase letters are the same as capitals
:SMALL-CAPS lowercase letters are smaller versions of the capitals
:OTHER lowercase letters are some other design")
   (char-range :type (member :alpha :alphanum :restricted :basic :extended)
               :documentation "A symbol indicating which glyphs are in this font.
:ALPHA alphabetical characters only.
:ALPHANUM alphabetical and numeric characters only
:RESTRICTED alphabetical and numeric characters as well as some symbols
:BASIC a basic set of alphabetical, numeric, and symbolic characters such as ASCII
:EXTENDED a larger range of characters, such as Latin-1 or Unicode"))
  (:documentation "An abstract description of a font in terms of general
characteristics. Many of the characteristics are expressed as a ratio to cap-height, which
is the height of a standard capital letter. A good character to measure this on is the
`H'. Note that this measurement, like all others, should be taken parallel to the slant of
the font. Slots for which no reasonable measurement is possible should be bound to NIL,
e.g., when the characteristic varies wildly from character to character."))

;;;
;;; Text Font characteristics
;;;

(defclass serif-style ()
  ()
  (:documentation "A dummy superclass to help keep the serif styles organized and easily
searchable."))

(defmacro define-serif-style (name superclasses &rest options)
  `(defclass ,name ,(or superclasses '(serif-style))
     ()
     ,@options))

(define-serif-style sans-serif ()
  (:documentation "Ends do not have a sudden widening at the tip."))

(define-serif-style flared (sans-serif)
  (:documentation "Ends have a gradual widening at the tip exceeding 5% of the central
width of the stem. If widening encompasses more than 35% of the cap height, it is likely
that the font is not flared, but simply has concave strokes."))

(define-serif-style rounded (sans-serif)
  (:documentation "Ends have a rounding at the tip which exceeds 20% of the width of the
stem on each side. (A tip which is completely elliptical or circular has 50% rounding.)"))

(define-serif-style perpendicular-sans-serif (sans-serif)
  (:documentation "Ends are not flared or rounded, but angled stems (e.g. on the `A')
terminate at an angle from the baseline."))

(define-serif-style obtuse-sans-serif (sans-serif)
  (:documentation "Ends are highly angular from the horizontal or vertical. The top right
end on the `E' is diagonal, covering more than 3% of the length of the top horizontal stem."))

(define-serif-style normal-sans-serif (sans-serif)
  (:documentation "All other sans-serif designs."))

(define-serif-style serif ()
  (:documentation "Ends have a sudden widening at the tip, often looking like there is a
separate stroke perpendicular to the main stroke."))

(define-serif-style exaggerated-serif (serif)
  (:documentation "These serifs have a more than 20% variation in size from one side of
the serif to another."))

(define-serif-style cove-serif (serif)
  (:documentation "Cove serifs are joined to the main stroke with a curve."))

(define-serif-style obtuse-cove-serif (cove-serif)
  (:documentation "Cove serifs which are highly angular in certain positions. The top
right serif on the `E' is diagonal, covering more than 7% of the length of the top
horizontal stem (including any half-serif at the top left) ."))

(define-serif-style square-cove-serif (cove-serif))
(define-serif-style obtuse-square-cove-serif (square-cove-serif obtuse-cove-serif))

(define-serif-style thin-serif (serif)
  (:documentation "The serifs are very thin in depth."))

(define-serif-style triangular-serif (serif)
  (:documentation "The serifs form a nearly triangular shape."))

(define-serif-style square-serif (serif)
  (:documentation "The serifs are square at the intersection of the main stroke."))

(define-serif-style oval-serif (serif)
  (:documentation "The serifs are mostly composed of curves."))

(defclass text-font-characteristics (font-characteristics)
  ((serif-style :type (or symbol standard-class)
                :documentation "The type of serifing of the font. A class metaobject that
is a subclass of serif-style or the name of such a class.")))

;;;
;;; Decorative font characteristics
;;;

(defclass stroke-style ()
  ()
  (:documentation "A dummy superclass to help keep the stroke styles organized and easily
searchable."))

(defmacro define-stroke-style (name superclasses &rest options)
  `(defclass ,name ,(or superclasses '(stroke-style))
     ()
     ,@options))

(define-stroke-style normal ()
  (:documentation "A normal stroke style, like that of any standard text font."))

(define-stroke-style outline ()
  (:documentation "Just an outline of a normal stroke is present."))

(define-stroke-style inline ()
  (:documentation "The strokes are shaded on the inside."))

(define-stroke-style college ()
  (:documentation "A solid shape with an additional outline."))

(define-stroke-style disconnected ()
  (:documentation "The character consists of multiple, disconnected segments."))

(define-stroke-style shadow ()
  (:documentation "A shadow copy is also present offset from the main stroke."))

(define-stroke-style handwritten ()
  (:documentation "A stroke that looks handwritten."))

(define-stroke-style brush (handwritten)
  (:documentation "A stroke that looks like it was written with a rectangular brush. It
may resemble calligrapic strokes, but has more fluidness and swellings."))

(define-stroke-style rough (handwritten)
  (:documentation "Strokes that may have multiple lines or terminate in what looks like
multiple lines, as though written with a rough nib, with a dry nib, or on a rough
surface. The stroke has unpredictable starts and stops."))

(define-stroke-style round-tip (handwritten)
  (:docimentation "Strokes that look as though written with a ballpoint pen or other
round-tipped nib with even line weights and rounded line ends."))

(define-stroke-style soft-tip (handwritten)
  (:documentation "Strokes that look as though written with a felt or brush tip. A look
similar to that of round-tip, but with swellings."))

(define-stroke-style flat-tip ()
  (:documentation "A stroke whose width changes dramatically with the angle of the line,
as though it were written with an inflexible, flat-nibbed pen like those used in
calligraphy."))

(define-stroke-style engraved ()
  (:documentation "Strokes with multiple lines, or with an offset empty region in the
middle of the strokes."))

(define-stroke-style copperplate ()
  (:documentation "Strokes that terminate by swelling into serif-like ends. Copperplate
Gothic is a common example. Is (almost?) always a small-caps or all-caps font."))

(defclass char-shape ()
  ()
  (:documentation "A dummy superclass to help keep the character shapes organized and
easily searchable."))

(defmacro define-stroke-style (name superclasses &rest options)
  `(defclass ,name ,(or superclasses '(char-shape))
     ()
     ,@options))

(define-char-shape roman ()
  (:documentation "The shape is the same as a normal roman typeface."))

(define-char-shape italic ()
  (:documentation "The shape is italic in style."))

(define-char-shape cursive ()
  (:documentation "The character shapes are those used when writing cursive text."))

(define-char-shape handwritten ()
  (:documentation "The character shapes are like simple hand-printed characters (not
cursive). Usually the `g' has a much simpler shape than in a roman font and sometimes the
`a', as well."))

(define-char-shape blackletter ()
  (:documentation "The characters have the shapes of the German Fraktur style."))

(define-char-shape deco ()
  (:documentation "The characters have very high or low midlines, often making characters
like `E', `M', and `S' have unusual shapes."))

(define-char-shape lombardic ()
  (:documentation "The stems are very exaggerated and manipulated."))

(defclass decorative-font-characteristics (font-characteristics)
  ((stroke-style :type (or symbol standard-class)
                 :documentation "The type of stroke used in the font. A class metaobject that
is a subclass of stroke-style or the name of such a class.")
   (char-shape :type (or symbol standard-class)
               :documentation "The shape of the characters in the font. A class metaobject that
is a subclass of char-shape or the name of such a class.")
   (connection-style :type (member :disconnected :trailing :connected)
                     :documentation "The way in which one character connects to the next. 
:disconnected Strokes of one character do not trail or connect with the next.
:trailing Strokes of characters extend and may overlap with strokes of the next.
:connected Strokes of one character explicitly connect to strokes of the next.")
   (wrapping-style :type (member :none :0-90 :90-360 :360+)
                   :documentation "The amount that the trailing end of the stroke, if any,
wraps around. Usually measured on the `D' as an angle that the stroke arcs through.")))

;;; Examples:
;;; Font Name - stroke-style, char-shape, connection-style, wrapping-style
;;; Comic Sans MS - round-tip, handwritten, disconnected, none
;;; Zapf Chancery - calligraphic, handwritten, disconnected, 90-360 deg
;;; Brush Script MT - brush, cursive, connected, 0-90 deg
;;; Colonna MT - engraved, roman, disconnected, none
;;; Copperplate Gothic - copperplate, roman?, disconnected, none
;;; Desdemona - outline, deco, disconnected, none
;;; Lucida Handwriting - soft-tip, handwritten, connected, 0-90 deg
;;; Lucida Casual - soft-tip, roman, disconnected, none
;;; Arnold B�cklin - normal, handwritten, disconnected, 360+ deg

(define-specified-type font
    :namespacep t
    :cannonical-types (named-font indirect-font logical-font font-characteristics))

(defconstant +numbered-font-table+ (make-array (expt 2 7)))

(defmethod font-p ((object fixnum))
  (if (< -1 object (expt 2 7))
      t
      (call-next-method)))

(define-specified-type-converter font fixnum
  (if (< -1 fixnum (expt 2 7))
      (aref +numbered-font-table+ fixnum)
      (call-next-method)))
