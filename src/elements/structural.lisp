(in-package :defdoc.elements)

(defclass logical-structure-element (vertical-element vertical-container
                                     flowing-container-mixin)
  ())

(defclass section (logical-structure-element)
  ((title :initarg title
          :accessor title))
  (:documentation "A major structural part of the document."))

(defclass section-header (logical-structure-element)
  ((header-level :initarg header-level
                 :accessor header-level
                 :documentation "The level of the header, with topmost level headers being
at level 1."))
  (:documentation "A section header object only to be created by layout engines."))

(defclass paragraph (logical-structure-element wrapping-container-mixin)
  ()
  (:documentation "A standard paragraph of text."))

(defclass block-quotation (logical-structure-element wrapping-container-mixin)
  ()
  (:documentation "A quotation displayed as a set-off block of text."))

(defclass listed-information (logical-structure-element)
  ()
  (:documentation "An abstract list of textual items."))

(defclass enumerated-list (listed-information)
  ()
  (:documentation "A list of items labelled with increasing indices."))

(defclass itemized-list (listed-information)
  ()
  (:documentation "A list of items labelled with a special character (bullet) to set each
one off."))

(defclass list-item (logical-structure-element)
  ()
  (:documentation "An item in a listed-information element"))

(defclass definition-list (listed-information)
  ()
  (:documentation "A list of terms and one or more definitions for each. The contents are
an alist. Each CAR is the term being defined, each CDR is the definition or list of
definitions."))

(defclass defined-term (list-item)
  ()
  (:documentation "An item in a definition-list: the term which will be defined"))

(defclass term-definition (logical-structure-element)
  ()
  (:documentation "An item in a definition-list: the definition of the term just introduced"))

(defclass preformatted-paragraph (logical-structure-element)
  ()
  (:documentation "A paragraph whose elements are already laid out according to a
monospaced font."))

