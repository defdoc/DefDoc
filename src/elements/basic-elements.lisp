(in-package :defdoc.elements)

;;;
;;; Basic elements
;;;

(defclass element ()
  ((height :type length
           :initarg height
           :documentation "The distance from the beginning of this box to the horizontal
baseline. Unbound means to fill the parent box in that direction.")
   (width :type length
          :initarg width
          :documentation "The distance from the beginning of this box to the vertical
baseline. Unbound means to fill the parent box in that direction.")
   (engine-specific :type list
                    :initform nil
                    :initarg engine-specific
                    :accessor engine-specific
                    :documentation "A alist of output or layout engine name to an alist of
engine-specific options. Use ENGINE-SPECIFIC-OPTION to access this slot's information
externally."))
  (:documentation "The basic type of an element."))

(defgeneric engine-specific-option (element engine option)
  (:documentation "Returns a the engine-specific option that applies to the given
conversion under the given name."))

(defun get-one-engine-specific (element engine option)
  (declare (type element element)
           (type symbol engine))
  (cdr (assoc option (cdr (assoc engine (engine-specific element))))))

(defmethod engine-specific-option ((element t) (engine t) (option t))
  nil)

(eval-when (:execute :compile-toplevel)
  (defconstant +mop-cpl+
      (first (find-all-symbols "CLASS-PRECEDENCE-LIST")))
  (defconstant +mop-class-of+
      (or (find-symbol "CLASS-OF" (symbol-package +mop-cpl+)) 'class-of))
  (defconstant +mop-class-name+
      (or (find-symbol "CLASS-NAME" (symbol-package +mop-cpl+)) 'class-name)))

(defmethod engine-specific-option ((element element) (engine t) (option t))
  (and (engine-specific element)
       (loop
           for class in (#.+mop-cpl+ (#.+mop-class-of+ engine))
           append (get-one-engine-specific element
                                           (#.+mop-class-name+ class)
                                           option))))

(defclass vertical-element (element)
  ((breadth :type length
            :initarg breadth
            :documentation "The distance from the vertical baseline to the end of this
box. Unbound means to fill the parent box in that direction."))
  (:documentation "An element that is to be laid out in a vertical sequence."))

(defclass horizontal-element (element)
  ((depth :type length
          :initarg depth
          :documentation "The distance from the horizontal baseline to the end of this
box. Unbound means to fill the parent box in that direction."))
  (:documentation "An element that is to be laid out in a horizontal sequence."))

;;;
;;; Containers
;;;

(defclass container (element)
  ((contents :type list
             :initform nil
             :initarg contents
             :accessor contents
             :documentation "The elements that are laid out in this container. They must
all have an orientation that is perpendicular to this container or be flowing-containers.")
   (position-offset :type length
                    :initform 0
                    :initarg position-offset
                    :accessor position-offset
                    :documentation "The offset of this container in the parent container,
in the direction perpendicular to the layout of this container. Currently ignored.")
   (layout-direction :type (member :right/down :left/up)
                     :initform :right/down
                     :initarg layout-direction
                     :accessor layout-direction
                     :documentation "The direction in which the elements in this container
should be laid out. Currently ignored.")
   (badness-weighting :type badness-weighting
                      :initarg badness-weighting
                      :accessor badness-weighting
                      :documentation "The treatment a badness is given in order to
determine the penalty assessed for layouts within this container. Currently ignored."))
  (:documentation "Mixin for elements that themselves contain elements. Do not use this
class to create a normal container element; rather, choose one of vertical- or
horizontal-container-mixin and any of the other mixins. Breaking is not performed on
simple containers."))

(defmethod slot-unbound
    ((class standard-class) (instance container) (slot-name (eql 'badness-weighting)))
  *badness-weighting*)

(defclass vertical-container (container)
  ()
  (:documentation "Mixin for containers whose contents are vertical-elements."))

(defclass horizontal-container (container)
  ()
  (:documentation "Mixin for containers whose contents are horizontal-elements."))

(defclass overflowable-container-mixin (container)
  ((overflow-containers :type list
                        :initform nil
                        :accessor overflow-containers
                        :documentation "The list of containers which, in order, will
accomodate overflowing elements from this container."))
  (:documentation "Mixin for a container whose contents overflow into a specific series of
containers. Useful for, e.g., articles in newspapers and magazines with
continuations. Used in implementing pages and lines as well."))

(defclass wrapping-container-mixin (container)
  ((sub-container-class :type (or standard-class symbol)
                        :initarg sub-container-class
                        :accessor sub-container-class
                        :documentation "Class of the sub-containers that this one wraps into."))
  (:documentation "Mixin for a container whose contents can be wrapped as desired into as
many consecutive similar containers within the parent container. E.g., a normal document
is a subclass of wrapping-container-mixin with a sub-container-constructor that makes page
objects. Note that paragraphs are flowing-vertical-containers because the lines in them
are laid out vertically and flow into the parent element's layout; it is the lines that
are horizontal-containers."))

(defclass flowing-container-mixin (container)
  ()
  (:documentation "Mixin for a container whose contents flow into (are spliced into the
layout of) the parent container's contents if they both have the same orientation.
Containers that do not inherit from this class will never be broken."))

(defclass floatable-container-mixin (container)
  ((float-penalty :type penalty
                  :initform 0
                  :initarg float-penalty
                  :accessor float-penalty
                  :documentation "The penalty of floating the container. Defaults to 0,
indicating to always float the container unless putting it inline creates a more pleasing layout.")
   (float-distance-penalty :type penalty
                           :initform *default-float-distance-penalty*
                           :initarg float-distance-penalty
                           :accessor float-distance-penalty
                           :documentation "The penalty of floating the container, charged
once each time that it is pushed into the next overflow container. To use a non-linear
relationship, override the CALCULATE-FLOAT-PENALTY generic-function in a subclass."))
  ;;; XXX Need a way to indicate that this float must come before some specific
  ;;; (higher-level) container type's end. Just have some way for a container type to
  ;;; indicate that all pending floats should be placed before it?
  
  ;;; To handle, e.g., footnotes, we need to have one of these that can be wrapped, and
  ;;; give a way to assess penalties, most notably for having more than a certain portion
  ;;; of a page filled with footnote information.
  (:documentation "Mixin to create a container which can be floated and pushed into
overflow containers."))

(defgeneric calculate-float-penalty (element distance)
                                        ;; XXX this needs more parameters, probably
  (:documentation "Computes the penalty to be assessed for floating the given ELEMENT the
given DISTANCE away from where it is defined in the document."))

(defmethod calculate-float-penalty ((element floatable-container-mixin) distance)
  (+ (float-penalty element) (* distance (float-distance-penalty element))))

;;;
;;; Rules
;;;

(defvar *default-line-rule-thickness* '(4/10 px))

(defclass rule-mixin ()
  ()
  (:documentation "An element that is filled with ink within the dimension boundaries."))

(defclass horizontal-rule (horizontal-element rule-mixin)
  ((height)))

(defmethod slot-unbound (class (instance horizontal-rule) (slot-name (eql 'height)))
  *default-line-rule-thickness*)

(defclass vertical-rule (vertical-element rule-mixin)
  ((width)))

(defmethod slot-unbound (class (instance vertical-rule) (slot-name (eql 'width)))
  *default-line-rule-thickness*)

;;;
;;; Characters
;;;

(defclass simple-character ()
  ((font :initarg font
         :accessor font)
   (code-number :type unsigned-byte
                :initarg code-number
                :accessor code-number
                :documentation "The index of the character in the specified font.")))

(deftype small-character ()
  "Only usable for characters whose font and code-number can be represented as small
numbers. 7 bits for the font, 8 bits for the character code."
  'fixnum)

(defmethod font ((char fixnum))
  (ldb (byte 7 8) char))

(defmethod code-number ((char fixnum))
  (ldb (byte 8 0) char))

(defmethod font ((char character))
  0)

(defmethod code-number ((char character))
  (char-code char))

(defclass ligature (simple-character)
  ((replaced-chars :type list
                   :initarg replaced-chars
                   :accessor replaced-chars
                   :documentation "The simple characters that were replaced, if any, in
order to form this ligature. Useful in, e.g., reconstructing the input text for
communication to the end-user or in outputting to a medium which does not have this (or
any) ligature."))
  (:documentation "A character that is formed from the interaction of multiple characters."))

;;;
;;; Breaking
;;;

(defclass penalty-node ()
  ((break-penalty :type penalty
                  :initarg break-penalty
                  :accessor break-penalty))
  (:documentation "A node indicating the penalty associated with breaking at this specific
location."))

(defclass discretionary-break (penalty-node)
  ((pre-break-elements :type list
                       :initform nil
                       :initarg pre-break-elements
                       :accessor pre-break-elements
                       :documentation "The text to precede the break if it is used.")
   (post-break-elements :type list
                        :initform nil
                        :initarg post-break-elements
                        :accessor post-break-elements
                        :documentation "The text to follow the break if it is used.")
   (no-break-elements :type list
                      :initform nil
                      :initarg no-break-elements
                      :accessor no-break-elements
                      :documentation "The text to be placed here if the break is not used.")
   #+nil
   (num-elements-replaced :type unsigned-byte
                          :initform nil
                          :initarg num-elements-replaced
                          :accessor num-elements-replaced
                          :documentation "The number of elements to be skipped if the break
is used. This is the way that Knuth's TeX program implements this data structure (instead
of the previous slot).")
   #+nil
   (break-skip-to :type list
                  :initarg break-skip-to
                  :accessor break-skip-to
                  :documentation "The cons containing the element to skip to if the
break is used. Another way of implementing the previous slot."))
  (:documentation "A location where a break can be placed, if necessary."))

(defmethod slot-unbound ((class t) (instance discretionary-break)
                         (slot-name (eql 'break-penalty)))
  *discretionary-break-penalty*)

(defclass discretionary-hyphen (discretionary-break)
  ((pre-break-elements #-pcl :type #-pcl (cons * null)
                       :reader pre-break-elements
                       :documentation "Initialize this with the 'HYPHEN-CHAR initarg.")
   (post-break-elements :type null :allocation :class)
   (no-break-elements :type null :allocation :class)
   (break-penalty :type null :allocation :class)))

#|
In LWPE:

Error: Defining method #<STANDARD-METHOD MAKE-INSTANCE (:AROUND) ((EQL #<STANDARD-CLASS
DISCRETIONARY-HYPHEN 2140C39C>)) 206696C4> visible from package COMMON-LISP.

Unfortunately, they signal a simple-error, so there's no way to reliably attach a
handler... unless I maybe look at the available restarts?
|#

(defmethod make-instance :around ((class (eql (find-class 'discretionary-hyphen)))
                                  &rest initargs)
  (restart-case
      (call-next-method)
    (try-discretionary-break () :report "Try making a DISCRETIONARY-BREAK instead."
      (incf (getf initargs 'break-penalty *discretionary-hyphen-penalty*) 0) ; ``ensure-property''
      (destructuring-bind (&key ((hyphen-char hyphen-char) #\- hyphen-char-p)
                                ((pre-break-elements pre-break) nil pre-break-p)
                           &allow-other-keys)
          initargs
        (declare (ignore pre-break))
        (cond
         ((and hyphen-char-p pre-break-p)
          (restart-case
              (error "Both a hyphen character and the pre-break elements have been specified.")
            (use-hyphen-char () :report "Use the hyphen character."
              (setf (getf initargs 'pre-break-elements) (list hyphen-char))
              (remf initargs 'hyphen-char))
            (use-pre-break-elements () :report "Use the pre-break elements."
              (remf initargs 'hyphen-char))))
         (pre-break-p)
         (t (setf initargs (list* 'pre-break-elements hyphen-char initargs))
            (remf initargs 'hyphen-char))))
      (apply #'make-instance 'discretionary-break initargs))))

(defmethod shared-initialize :around ((object discretionary-hyphen) slots
                                      &rest initargs
                                      &key ((hyphen-char hyphen-char) #\-))
  (declare (ignore slots))
  (when (getf initargs 'break-penalty)
    (cerror "Don't set it."
            "Attempt to set break penalty for a discretionary hyphen in an instance's
initargs. Please either set *DISCRETIONARY-HYPHEN-PENALTY* to change it globally or use
the more generic DISCRETIONARY-BREAK.")
    (remf initargs 'break-penalty))
  (when (getf initargs 'pre-break-elements)
    (cerror "Don't set it."
            "Attempt to set pre-break elements for a discretionary hyphen. Please use the
more generic DISCRETIONARY-BREAK for this functionality.")
    (remf initargs 'pre-break-elements))
  (when (getf initargs 'post-break-elements)
    (cerror "Don't set it."
            "Attempt to set post-break elements for a discretionary hyphen. Please use the
more generic DISCRETIONARY-BREAK for this functionality.")
    (remf initargs 'post-break-elements))
  (when (getf initargs 'no-break-elements)
    (cerror "Don't set it."
            "Attempt to set no-break elements for a discretionary hyphen. Please use the
more generic DISCRETIONARY-BREAK for this functionality.")
    (remf initargs 'no-break-elements))
  (call-next-method object slots 'pre-break-elements (list hyphen-char) initargs))

(defmethod break-penalty ((object discretionary-hyphen))
  *discretionary-hyphen-penalty*)

(defmethod (setf break-penalty) (new-value (object discretionary-hyphen))
  (setf new-value *discretionary-hyphen-penalty*))

;;;
;;; Springs
;;;

(deftype springiness-priority ()
  '(signed-byte 8))

(defclass fixed-springiness ()
  ((fixed-length :type length
                 :initarg :amount
                 :accessor fixed-length)
   (priority :type springiness-priority
             :initform 0
             :initarg :priority
             :accessor priority))
  (:documentation "A fixed-size springiness."))

(defclass relative-springiness ()
  ((springiness-portion :type rational
                        :initarg :amount
                        :accessor springiness-portion)
   (priority :type springiness-priority
             :initform 0
             :initarg :priority
             :accessor priority))
  (:documentation "A fixed-size springiness whose size is a factor of the dimensions of
the associated element."))

(defclass infinite-springiness ()
  ((springiness-portion :type rational
                        :initarg :amount
                        :accessor springiness-portion)
   (priority :type springiness-priority
             :initform 0
             :initarg :priority
             :accessor priority))
  (:documentation "Infinite springiness takes priority over all other types and can
stretch or shrink infinitely."))

(define-specified-type springiness
    :namespacep t
    :cannonical-types (fixed-springiness infinite-springiness relative-springiness))

(defspringiness :none
    (make-instance 'fixed-springiness :amount (make-length 0 t))
  "A non-springy springiness.")

(defclass springy-mixin ()
  ((stretch :initform (springiness :none)
            :initarg stretch
            :accessor stretch)
   (shrink :initform (springiness :none)
           :initarg shrink
           :accessor shrink)))

(defclass spring (springy-mixin)
  ((natural-length :type length
                   :initform 0
                   :initarg natural-length
                   :accessor natural-length)))

(define-specified-type spring
    :namespacep t)

(defstruct spring-total
  "Type expressing the total amount of length and springiness in a number of elements. No
units are given, since the measurements should be converted to a common unit before
accumulation."
  (units t :type symbol)
  (length 0 :type rational)
  (fixed-stretch '() :type list #| alist of priority, total |#)
  (fixed-shrink '() :type list #| alist of priority, total |#)
  (infinite '() :type list #| alist of priority, total |#))

(defgeneric accumulate-springiness (spring-total springiness sign))

(defmethod accumulate-springiness ((spring-total spring-total)
                                   (springiness fixed-springiness)
                                   sign)
  (let ((value (convert-length (fixed-length springiness) (spring-total-units spring-total)))
        (priority (priority springiness)))
    (ecase sign
      (+1 (incf (getf (spring-total-fixed-stretch spring-total) priority 0)
                value))
      (-1 (incf (getf (spring-total-fixed-shrink spring-total) priority 0)
                value)))))

(defun accumulate-spring (spring-total spring)
  (declare (type spring-total spring-total))
  (incf (spring-total-length spring-total) (fixed-length spring))
  (accumulate-springiness spring-total (stretch spring) +1)
  (accumulate-springiness spring-total (shrink spring) -1)
  spring-total)

(defun springiness-spec-to-springiness-creation-form (amount units priority)
  (let ((springiness-class
         (cond ((string-equal (string units) "INF")
                'infinite-springiness)
               ((string-equal (string units) "%")
                (setf amount (/ amount 100))
                'relative-springiness)
               (t (setf amount (make-length amount units))
                  'fixed-springiness))))
    `(make-instance ',springiness-class :amount ',amount :priority ',priority)))

(defmacro make-spring (natural-amount natural-units &rest rest)
  "The syntax is: ((natural-amount natural-units (direction [ name / (amount units
priority?)]{0,2}))) where DIRECTION is a symbol with a name of either + or - (at most one
of each allowed per definition) to indiciate whether the following items define stretching
or shrinking, and PRIORITY is an optional numeric identifier of how much precedence the
stretch or shrink should take over others. A priority of 0 is normal, a priority of 1
indicates that this one should be used before all those of priority 0, etc. Priorities
from -127 to 128 are allowed. The UNITS of a stretch or shrink may be the name of any
defined length unit (see: define-length-unit), % to indicate springiness relative to the
natural length, or INF to indicate an infinite amount of stretch or shrink. If a NAME is
given instead, it is looked up in the springiness namespace (see: defspringiness)."
  (declare (type real natural-amount)
           (type symbol natural-units))
  (let ((stretch nil)
        (shrink nil))
    (when rest
      (loop repeat 2
          while rest
          do (let ((direction (find-symbol (symbol-name (pop rest)) #.*package*))
                   (amount (pop rest))
                   creation-form)
               (etypecase amount
                 (real (let ((units (pop rest))
                             (priority (and (numberp (car rest))
                                            (pop rest))))
                         (setf creation-form
                               (springiness-spec-to-springiness-creation-form
                                (rationalize amount) units priority))))
                 (symbol (setf creation-form `(springiness ,amount))))
               (ecase direction
                 (+ (if stretch
                        (error "Attempting to create spring with two stretches.")
                        (setq stretch creation-form)))
                 (- (if shrink
                        (error "Attempting to create spring with two shrinks.")
                        (setq shrink creation-form)))))))
    (when rest
      (cerror "Ignore it."
              "Junk in MAKE-SPRING form: ~A." rest))
    `(make-instance 'spring
       'natural-length ',(make-length (rationalize natural-amount) natural-units)
       ,@(and stretch `('stretch ,stretch))
       ,@(and shrink `('shrink ,shrink)))))

;;;
;;; Kerns
;;;

(defclass kern ()
  ((width :type length
          :initarg width
          :accessor width))
  (:documentation "A small adjustment of spacing to make the surrounding letter shapes
flow more aesthetically. Usually automatically added based on font metric information; for
other situations, please define or use a subclass."))

(defclass explicit-kern (kern)
  ()
  (:documentation "A kern which was explicitly added to the text."))

(defclass accent-kern (kern)
  ()
  (:documentation "A kern which was added in order to make an accent character line up
with the accented character."))

