(defpackage :defdoc.elements
  (:use :defdoc :defdoc.layout-core :common-lisp :specified-types)
  (:intern #:section-header #:header-level)
  (:export
   ;;; basic-elements.lisp
   #:element
   #:engine-specific-option
   #:hight
   #:width
   #:breadth
   #:depth
   #:vertical-element
   #:horizontal-element
   
   #:container
   #:contents
   #:position-offset
   #:layout-direction
   #:badness-weighting
   #:default-badness-weighting
   #:*default-badness-weighting*
   #:vertical-container-mixin
   #:horizontal-container-mixin
   #:overflowable-container-mixin
   #:overflow-containers
   #:wrapping-container-mixin
   #:sub-container-class
   #:flowing-container-mixin
   #:floatable-container-mixin
   #:float-penalty
   #:float-distance-penalty
   #:caluclate-float-penalty
   
   #:rule-mixin
   #:horizontal-rule
   #:vertical-rule
   
   #:simple-character
   #:font
   #:code-number
   #:small-character
   #:ligature
   #:replaced-chars
   
   #:penalty-node
   #:discretionary-break
   #:pre-break-elements
   #:post-break-elements
   #:no-break-elements
   #:break-penalty
   #:discretionary-hyphen
   
   #:springiness-priority
   #:fixed-springiness
   #:fixed-length
   #:infinite-springiness
   #:springiness-portion
   #:relative-springiness
   #:springiness
   #:springiness-p
   #:springy-mixin
   #:stretch
   #:shrink
   #:spring
   #:make-spring
   #:natural-length
   
   #:kern
   #:explicit-kern
   #:accent-kern
   
   ;;; toplevel.lisp
   #:toplevel-element
   #:toplevel-subelement
   #:document
   #:title
   #:subtitle
   #:author
   #:copyright
   #:date
   #:page
   #:report
   #:book
   #:article
   #:presentation
   #:slide
   
   ;;; structural.lisp
   #:logical-structure-element
   #:section
   #:title
   #:paragraph
   #:block-quotation
   #:listed-information
   #:enumerated-list
   #:itemized-list
   #:definition-list
   #:preformatted-paragraph
   
   ;;; style.lisp
   #:bold
   #:italic
   #:small-caps

   ;;; abbreviations.lisp
   #:abbreviation
   #:defabbreviation

   ;;; links.lisp
   #:link
   #:url))

(defpackage :defdoc.elements.section-header
  (:use)
  (:import-from :defdoc.elements #:section-header #:header-level)
  (:export #:section-header #:header-level))
