(in-package :defdoc.elements)

(defclass abbreviation (horizontal-element horizontal-container
                        flowing-container-mixin)
  ()
  (:documentation "Base class for all abbreviations."))

(defmacro defabbreviation (name &rest expansion)
  `(prog1
    (defclass ,name (abbreviation)
      ((contents :type null :allocation :class)))
    (let ((.value. (list ,@expansion)))
      (defmethod contents ((instance ,name))
        .value.))))

(defmethod shared-initialize :around ((instance abbreviation) slots &rest keys)
  (remf keys 'contents)
  (call-next-method instance slots keys))
