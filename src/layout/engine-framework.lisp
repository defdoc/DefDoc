(in-package :defdoc.layout)

;;; PREPARE
(defgeneric prepare (input output)
  (:documentation "Breaks up the input into the basic elements to be laid out and appends
them to the main vertical sequence using APPEND-TO-VERTICAL-SEQUENCE."))

(declaim (type list *vertical-sequence* *vertical-sequence-tail*))
(defvar *vertical-sequence*)
(defvar *vertical-sequence-tail*)

(declaim (type unsigned-byte *header-level*))
(defvar *header-level*)

(defun append-to-vertical-sequence (list)
  "Appends the given list of elements to the main vertical sequence."
  (if *vertical-sequence*
      (setf (rest *vertical-sequence-tail*) list)
      (setf *vertical-sequence* list))
  (setf *vertical-sequence-tail* (last list)))

(defmethod prepare ((input t) (output t))
  "By default, just leave it as-is."
  (append-to-vertical-sequence (list input)))

(defmethod prepare ((input toplevel-element) (output t))
                                        ; XXX TODO: title / title page
  (dolist (element (contents input))
    (prepare element output)))

#+nil
(defmethod prepare :around ((input container) (output t))
  (let ((badness-weighting nil))
    (handler-case (setf badness-weighting (badness-weighting input))
      (slot-unbound ()))
    ;;(progv (and badness-weighting '(*badness-weighting*))
    ;;    (list badness-weighting)
    (let ((*badness-weighting* (or badness-weighting *badness-weighting*)))
      (call-next-method))))

;;; COMPUTE-BREAKS
(defgeneric compute-breaks (input elements output)
  (:documentation "Breaks up the given basic element list, returning a list of
toplevel-subelements."))

(defstruct (active-break
            (:constructor make-active-break
                          (breakpoint total-penalty &key fitting discretionary-p #+nil number)))
  (breakpoint nil :type (or null passive-break) :read-only t)
  (total-penalty 0 :type penalty :read-only t) ; the total penalty up to this breakpoint
  #+nil(number 0 :type integer :read-only t) ; the line, page, etc number this break starts
  (fitting nil :read-only t)            ; for assessing *adjacent-incompatible-fitting-penalty*
  (discretionary-p nil :type boolean :read-only t))

(declaim (type unsigned-byte *passive-break-counter*))
(defvar *passive-break-counter*)

(defstruct (passive-break (:constructor make-passive-break (breakpoint previous)))
  (breakpoint nil :type list :read-only t) ; its CAR is the item broken on or nil if the
                                        ; start or end of the paragraph.
  (previous nil :type (or null passive-break) :read-only t) ; the break which would best precede this one
  (serial (incf *passive-break-counter*) :type unsigned-byte :read-only t))

(defmethod compute-breaks :around ((input toplevel-element) (elements t) (output t))
  (let* ((*passive-break-counter* 0))
    (call-next-method)))

(defun compute-penalty-form (location force-p active badness penalty fitting fittings)
  `(if ,force-p
       (active-break-total-penalty ,active)
       (penalty+ (active-break-total-penalty ,active)
                 ,badness *per-break-penalty* ,penalty
                 (if (active-break-discretionary-p ,active)
                     (cond
                      ((typep (first ,location) 'discretionary-break)
                       *consecutive-discretionary-break-penalty*)
                      ((null ,location) *final-break-discretionary-penalty*)
                      (t 0))
                     0)
                 ,(when fittings
                    `(if (not (eql (active-break-fitting ,active) ,fitting))
                         *adjacent-incompatible-fitting-penalty*
                         0)))))

(defun compute-breaking-function (spec tolerance num-fittings)
  "Tests if the breakpoint at the start of LOCATION is feasible by running through
ACTIVE-LIST and measuring the penalty for using each of those breakpoints as the previous
one. If LOCATION is NIL, we are at the end of the container. ACTIVE-LENGTH is the distance
from the first active break to the current location. It is maintained by the breaking
function. The return value is the new ACTIVE-LIST to be used for later breakpoints,
removing ones that are too far from the current element and adding one for the current
element when appropriate."
  (let ((minimal-penalty (gensym "MINIMAL-PENALTY-"))
        (minimum-penalty (gensym "MINIMUM-PENALTY-"))
        (best-place (gensym "BEST-PLACE-"))
        (collect-p (gensym "COLLECT-P-"))
        (record-p (gensym "RECORD-P-"))
        (new-active-list (gensym "NEW-ACTIVE-LIST-"))
        (location (gensym "LOCATION-"))
        (penalty (gensym "PENALTY-"))
        (active-list (gensym "ACTIVE-LIST-"))
        (active (gensym "ACTIVE-"))
        (delta (gensym "DELTA-"))
        (fittings-p (plusp num-fittings)))
    (destructuring-bind
        (name (current-active-length active-length &optional break-length)
         (natural-size stretch shrink) (badness direction) fitting-form)
        spec
      `(,name
        (,location ,penalty ,active-list)
        (let ((,current-active-length ,active-length)
              ,@(when break-length `((,break-length 0)))
              (,minimum-penalty t)
              (,new-active-list nil)
              ,@(if fittings-p
                    `((,minimal-penalty
                       (make-array ,num-fittings :element-type 'penalty
                                   :initial-element t))
                      (,best-place
                       (make-array ,num-fittings :element-type '(or null passive-break)
                                   :initial-element nil)))
                    `((,best-place))))
          (do ((,active #1=(pop ,active-list) #1#))
              ((null ,active))
            (if (integerp ,active)
                (progn
                  (incf ,current-active-length ,active)
                  (push ,active ,new-active-list))
                (let ((,collect-p t)
                      (,record-p nil))
                  (multiple-value-bind (,badness ,direction)
                      (compute-badness ,current-active-length
                                       ,natural-size ,stretch ,shrink)
                    (let ((.fitting. ,fitting-form))
                      (if (or (>= ,badness +infinitely-bad+) (penalty= ,penalty nil))
                          (progn        ; 854: prepare to deactivate active
                            (cond
                             ((and (eq ,tolerance t)
                                   (eq ,minimum-penalty t)
                                   (null (rest ,new-active-list))
                                   (null ,active-list))
                                        ; OK, we give up. Let's just use this break, it's
                                        ; the only one we have.
                              (setf ,record-p :force))
                             ((penalty<= ,badness ,tolerance) (setf ,record-p t)))
                            (setf ,collect-p nil))
                          (unless (penalty> ,badness ,tolerance) (setf ,record-p t)))
                      (when ,record-p   ; 855
                        (let ((.total-penalty.
                               ,(compute-penalty-form location `(eq ,record-p :force)
                                                      active badness penalty
                                                      '.fitting. fittings-p)))
                          ,(if fittings-p
                               `(when (penalty<= .total-penalty.
                                                 (aref ,minimal-penalty .fitting.))
                                  (setf (aref ,minimal-penalty .fitting.) .total-penalty.)
                                  (setf (aref ,best-place .fitting.)
                                        (active-break-breakpoint ,active))
                                  (setf ,minimum-penalty
                                        (penalty-min ,minimum-penalty .total-penalty.)))
                               `(when (penalty<= .total-penalty. ,minimum-penalty)
                                  (setf ,minimum-penalty .total-penalty.)
                                  (setf ,best-place (active-break-breakpoint ,active))))))
                      (if ,collect-p
                          (push ,active ,new-active-list)
                                        ; 860: fixup the deltas since we're not collecting
                                        ; an active-break
                          (cond
                           ((null ,new-active-list) ; 861: that was the first active break
                            (when (integerp (first ,active-list))
                              (incf ,active-length (pop ,active-list))
                              (setf ,current-active-length ,active-length)))
                           ((integerp (first ,active-list)) ; we just passed a delta
                            (cond
                             ((null ,active-list) ; and we're at the end
                              (decf ,current-active-length (pop ,new-active-list)))
                             ((integerp (first ,active-list)) ; and delta is just ahead
                              (let ((,delta (pop ,active-list)))
                                (incf ,current-active-length ,delta)
                                (incf (first ,new-active-list) ,delta))))))))))))
          (when (penalty< ,minimum-penalty t)
                                        ; 835/836: Create new active nodes for the best
                                        ; feasible breaks found. Do not compute
                                        ; break-length, since we don't yet have any
                                        ; elements that are discarded after breaking
            (cond                       ; 843: insert a delta to prepare for breaks here
             ((integerp (first ,new-active-list)) ; modify an existing delta
              ,(when break-length `(incf (first ,new-active-list) ,break-length)))
             ((null ,new-active-list)   ; no delta needed at the beginning
              (setf ,active-length ,(or break-length 0)))
             (t (push (- ,@(when break-length `(,break-length)) ,current-active-length)
                      ,new-active-list)))
            ,(if fittings-p             ; XXX be sure to add discretionary-p args below
                 `(progn
                    (penalty-incf ,minimum-penalty *adjacent-incompatible-fitting-penalty*)
                    (dotimes (.fitting. ,num-fittings)
                      (when (penalty<= (aref ,minimal-penalty .fitting.) ,minimum-penalty)
                                        ; 845
                        (let ((.new-passive.
                               (make-passive-break ,location (aref ,best-place .fitting.))))
                          (push (make-active-break .new-passive.
                                                   (aref ,minimal-penalty .fitting.)
                                                   :fitting .fitting.)
                                ,new-active-list)))))
                 `(unless (penalty= ,minimum-penalty t) ; 845
                    (let ((.new-passive. (make-passive-break ,location) ,best-place))
                      (push (make-active-break .new-passive. ,minimum-penalty)
                            ,new-active-list)))))
                                        ; 844: TODO, when variable container boundaries
                                        ; are implemented
          (nreverse ,new-active-list))))))

(defmacro with-breaking (((tolerance &rest fittings)
                          bindings
                          &rest breaking-functions)
                         &body body)
  (let ((num-fittings 0))
    `(let (,@(mapcar (lambda (fitting) `(,fitting ,(1- (incf num-fittings))))
                     fittings))
       (let (,@bindings)
         (flet (,@(mapcar (lambda (spec)
                            (compute-breaking-function spec tolerance num-fittings))
                    breaking-functions))
           ,@body)))))

;;; MEASURE
(defgeneric measure (element units)
  (:documentation "Measures the given element in terms of the given units. Returns NIL if
such a measurement is not possible."))

(defmethod measure ((element t) (units symbol))
  (measure element (search-for-unit units)))

(defmethod measure ((element t) (units null))
  (error "Attempt to measure the length of ~A in terms of unknown units." element))

(defvar *word-count-cache*)

(defun word-count (element)
  "Very roughly count the number of words in the given document element (based mostly on
the number of spaces)."
  (or (gethash element *word-count-cache*)
      (let ((words 0))
        (typecase element
          (string (incf words (count #\space element)))
          (list (incf words (reduce #'+ (mapcar #'word-count element))))
          (logical-structure-element (incf words (1+ (word-count (contents element)))))
          (container (incf words (word-count (contents element))))
          (character (when (eql element #\space) (incf words))))
        (setf (gethash element *word-count-cache*) words))))

(defmethod measure ((element t) (units (eql (search-for-unit 'words))))
  (word-count element))

;;; CONVERT
(defgeneric convert (input output)
  (:documentation "Converts the input into the format specified by the output."))

(defun layout (input output)
  "Lays out the input object for the layout parameters specified by the output object,
returning the objects that result."
  (let ((*vertical-sequence* '())
        (*vertical-sequence-tail* '())
        (*header-level* 1))
    (prepare input output)
    (compute-breaks input *vertical-sequence* output)))

(defmethod convert :around ((input toplevel-element) (output t))
  (let ((*print-length* nil)
        (*print-circle* nil)
        (*print-escape* nil)
        (*print-readably* nil)
        (*print-base* 10)
        (*print-radix* nil)
        (*print-lines* nil))
    (call-next-method))
  t)

(defmethod convert ((input toplevel-element) (output t))
  (dolist (toplevel-subelement (layout input output))
    (convert toplevel-subelement output)))

(defmethod convert :around ((input toplevel-element) (output pretty-printable-mixin))
  (let ((*print-right-margin* (print-right-margin output))
        (*print-miser-width* (print-miser-width output))
        (*print-pretty* (print-pretty output)))
    (call-next-method))
  t)

(defmethod convert ((input defdoc.elements:abbreviation) (output t))
  (dolist (element (contents input))
    (convert element output)))