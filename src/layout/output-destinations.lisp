(in-package :defdoc.layout)

(defclass output-destination ()
  ((output-filename :type string
                    :initform ""
                    :initarg output-filename
                    :accessor output-filename
                    :documentation "The filename to send the output to (or possibly some
identifier, like a URI, if it is not actually going to a file).")
   (open-file-args :initform nil
                   :initarg open-file-args
                   :accessor open-file-args
                   :documentation "Any additional arguments to pass when opening the
output file or stream.")
   (output-stream :type (or null stream (function (output-destination) stream))
                  :initform nil
                  :initarg output-stream
                  :accessor output-stream
                  :documentation "If this is non-nil, it is a stream or function to create
a stream, given the output-destination object, to which the output for the layout goes,
and the output-filename is simply a token to identify it.")
   (auxiliary-outputs :type list
                      :initform ()
                      :initarg auxiliary-outputs
                      :accessor auxiliary-outputs))
  (:documentation "A destination for output of the layout engine."))

;;;
;;; Mixins to give specific layout methods to target formats
;;;

(defclass fixed-layout ()
  ((page-width :type length
               :initarg page-width
               :accessor page-width)
   (page-height :type length
                :initarg page-height
                :accessor page-height))
  (:documentation "Formats which have fixed layout parameters. This means that the exact
visual layout must be computed to use this format and the target medium is a set of
fixed-sized regions. Usually these are physical media."))

(defclass fixed-width-layout ()
  ((page-width :type length
               :initarg page-width
               :accessor page-width)
   (page-height :type spring
                :initarg page-height
                :accessor page-height))
  (:documentation "Formats which have a fixed width but flexible length. Usually these are
virtual media without any intrinsic layout capabilities."))

(defclass fixed-height-layout ()
  ((page-height :type length
                :initarg page-height
                :accessor page-height)
   (page-width :type spring
               :initarg page-width
               :accessor page-width))
  (:documentation "Formats which have a fixed length but flexible width. Usually these are
virtual media without any intrinsic layout capabilities. Added for completeness."))

(defclass flexible-layout ()
  ((page-size :initform (make-spring 0 words + 1 inf)
              :type spring
              :initarg page-size
              :accessor page-size))
  (:documentation "Formats which have flexible layout parameters. The target medium's
regions will grow or shrink (and reflow) in all dimensions in order to accomodate the
layout choices made. Usually these are virtual media with some basic intrinsic layout
capabilities."))

(defclass no-layout ()
  ()
  (:documentation "Formats which need no layout in order to be used. Usually some other
application does the actual layout."))

;;;
;;; Physical output destinations (files, streams, etc)
;;;

(defclass output-pages-are-files ()
  ((output-filename :documentation "The filename prefix for the files of the output.")
   (output-file-suffix :type string
                       :initform ""
                       :initarg output-file-suffix
                       :accessor output-file-suffix
                       :documentation "The filename suffix for the files of the output.")
   (file-number-counter :type unsigned-byte
                        :accessor file-number-counter
                        :initform 0)
   (file-number-base :type unsigned-byte
                     :initarg file-number-base
                     :accessor file-number-base
                     :initform 10))
  (:documentation "Formats whose pages exist in separate files."))

(defgeneric get-next-filename (output)
  (:documentation "Gets the filename for the next (or only) page of an
output-destination."))

(defmethod get-next-filename ((output output-destination))
  (output-filename output))

(defmethod get-next-filename :around ((output output-pages-are-files))
  (with-output-to-string (out)
    (write-string (call-next-method) out)
    (write (incf (file-number-counter output))
           :stream out
           :radix nil
           :base (file-number-base output))
    (write-string (output-file-suffix output) out)))

(defgeneric get-next-output-stream (output)
  (:documentation "Gets the stream for the next (or only) page of an
output-destination."))

(defmethod get-next-output-stream ((output output-destination))
  (or (output-stream output)
      (apply 'open (get-next-filename output) :direction :output
             (open-file-args output))))

(defmacro with-next-output-stream ((var output) &body body)
  `(with-open-stream (,var (get-next-output-stream ,output))
     ,@body))
