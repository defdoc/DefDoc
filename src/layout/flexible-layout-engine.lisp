(in-package :defdoc.layout.flexible-layout)

;;; Breaking is only done in one dimension. We do not wrap wrapping-containers. We append
;;; most flowing-containers in the main vertical sequence to the document to be laid-out,
;;; then break where appropriate in that, following TeX's line-breaking algorithm... more
;;; or less, since we have flexibly-sized pages and probably won't have discretionary
;;; breaks (at least not yet), definitely don't have kerns or springs (glues),
;;; etc. However, we will need to be able to count ``words'' in code, math, and images.

(defmethod prepare ((input section) (output flexible-layout))
  "Break sections into the header and each of the items in the contents."
  (let ((section-header (make-instance 'section-header
                          'header-level *header-level* 'contents (title input)))
        (*header-level* (1+ *header-level*))
        (contents (contents input)))
    (do* ((completed-penalty *pre-heading-penalty*
                             previous-previous-penalty)
          (completed-element section-header
                             previous-element)
          (previous-previous-penalty *lone-heading-penalty*
                                     previous-penalty)
          (previous-element (pop contents)
                            current-element)
          (previous-penalty *lone-item-penalty*
                            current-penalty)
          (current-element (pop contents)
                           (pop contents))
          (current-penalty *lone-pair-penalty*
                           0)
          (dummy                        ; test for empty or one-item section body
           (cond
            ((null previous-element)
             (append-to-vertical-sequence
              (list (make-instance 'penalty-node 'break-penalty completed-penalty)))
             (prepare completed-element output)
             (return))
            ((null current-element)
             (append-to-vertical-sequence
              (list (make-instance 'penalty-node
                      'break-penalty completed-penalty)))
             (prepare completed-element output)
             (append-to-vertical-sequence
              (list (make-instance 'penalty-node
                      'break-penalty (+ previous-previous-penalty *lone-item-penalty*))))
             (prepare previous-element output)
             (return)))
           dummy))
        ((null current-element)         ; termination test
                                        ; termination forms
         (append-to-vertical-sequence
          (list (make-instance 'penalty-node
                  'break-penalty (+ completed-penalty *lone-pair-penalty*))))
         (prepare completed-element output)
         (append-to-vertical-sequence
          (list (make-instance 'penalty-node
                  'break-penalty (+ previous-previous-penalty *lone-item-penalty*))))
         (prepare previous-element output))
                                        ; iteration forms
      (append-to-vertical-sequence
       (list (make-instance 'penalty-node 'break-penalty completed-penalty)))
      (prepare completed-element output))))

(defun compute-page-words (page-size)
  (with-accessors ((page-natural-length natural-length)
                   (page-stretch stretch)
                   (page-shrink shrink)) page-size
    (assert (unit= 'word (length-units page-natural-length)))
    (let ((page-natural-words (length-amount page-natural-length)))
      (values
       page-natural-words
       (etypecase page-stretch
         (fixed-springiness
          (let ((length (fixed-length page-stretch)))
            (assert (or (zerop (length-amount length))
                        (unit= 'word (length-units length))))
            (length-amount length)))
         (relative-springiness
          (* page-natural-words (springiness-portion page-stretch)))
         (infinite-springiness t))
       (etypecase page-shrink
         (fixed-springiness
          (let ((length (fixed-length page-shrink)))
            (assert (or (zerop (length-amount length))
                        (unit= 'word (length-units length))))
            (length-amount length)))
         (relative-springiness
          (* page-natural-words (springiness-portion page-shrink)))
         (infinite-springiness
          (warn "Infinite shrinkage ~A found in page-size. Using 20% instead." page-shrink)
          (* 1/5 page-natural-words)))))))

(defmethod compute-breaks ((input toplevel-element) elements (output flexible-layout))
  (let* ((*word-count-cache* (make-hash-table :test 'eq)))
    (multiple-value-bind (page-natural-words page-stretch-words page-shrink-words)
        (compute-page-words (page-size output))
      (break-into-pages
       input
       elements
       (loop                            ; 863
           for tolerance in
             (list *ideal-badness-tolerance* *acceptable-badness-tolerance* t)
           count t into pass
           when
             (with-breaking ((tolerance decent tight very-tight loose)
                             ((active-list (list (make-active-break nil 0 :fitting decent)))
                              (active-length 0))
                             (try-break 
                              (current-active-length active-length)
                              (page-natural-words page-stretch-words page-shrink-words)
                              (badness direction)
                              (cond
                               ((<= badness 12) decent)
                               ((and (= direction -1) (> badness 99)) very-tight)
                               ((= direction +1)
                                (when (> badness 200) ; a bit wasteful, since we just calculated badness
                                  (setf badness (1+ +infinitely-bad+)))
                                loose)
                               (t tight))))
               (do* ((remaining-elements elements (rest remaining-elements))
                     (previous-element nil current-element)
                     (current-element #1=(first remaining-elements) #1#))
                   ((or (null active-list) (null current-element)))
                 (typecase current-element
                   (container (incf active-length (measure current-element 'words)))
                   (discretionary-break ; XXX TODO
                    )
                   (penalty-node
                    (block skip-element
                      (with-accessors ((penalty break-penalty)) current-element
                        (when (= pass 1) ; PREPARE doesn't always combine adjacent
                                        ; penalties, so do that on the first pass
                          (if (null previous-element)
                              (loop for next on (rest remaining-elements)
                                  while (typep (first next) 'penalty-node)
                                  finally (progn (setf elements next)
                                                 (return-from skip-element)))
                              (loop
                                  for next on (rest remaining-elements)
                                  while (typep (first next) 'penalty-node)
                                  do (penalty-incf penalty (break-penalty (first next)))
                                  do (setf (rest remaining-elements) (rest next)))))
                        (assert (and previous-element
                                     (not (typep previous-element 'penalty-node))))
                        (unless (or (null previous-element)
                                    (typep previous-element 'penalty-node)
                                    (penalty= t penalty))
                          (setf active-list
                                (try-break remaining-elements penalty active-list))))))))
                                        ; Now try the break at the end of the container.
                                        ; If we can't find a reasonable breaking sequence,
                                        ; this returns NIL.
               (if (and active-list (setf active-list (try-break nil nil active-list)))
                                        ; (extremum #'penalty< active-list
                                        ; :key #'active-break-total-penalty)
                   (loop                ; find the active-break with the least penalty
                       with best-break of-type active-break = (pop active-list)
                       with best-penalty = (active-break-total-penalty best-break)
                       for this-break of-type active-break in active-list
                       for this-penalty = (active-break-total-penalty this-break)
                       do (when (penalty< this-penalty best-penalty)
                            (setf best-break this-break
                                  best-penalty this-penalty))
                       finally (return best-break))
                   (when (= pass 3)
                     (error "Oops... we didn't find any breaking sequence... that's a problem..."))))
           return it)))))

(defun break-into-pages (input elements active) ; 877
  (declare (type active-break active))
  (let ((breaks nil)
        (page-class (sub-container-class input)))
    (loop                               ; 878
        for break = (active-break-breakpoint active) then (passive-break-previous break)
        while break
        do (push break breaks))
                                        ;(break "doing the page breaking!")
    (loop
        for break in breaks
        for location = (passive-break-breakpoint break)
        ;do (print "...New Page...")
        collect (loop
                    until (eq elements location)
                    for element = (pop elements)
                    when (not (typep element 'penalty-node))
                    collect element into page
                    ;and do (print element)
                    end
                    finally (return (make-instance page-class
                                      'contents page
                                      'toplevel-element input))))))
