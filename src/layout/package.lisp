(defpackage :defdoc.layout
  (:use :defdoc :defdoc.layout-core :defdoc.elements :defdoc.elements.section-header
        :common-lisp)
  #.`(:export
      ;;; re-export the layout-core symbols to keep it easy to use the layout engine
      ,@(loop for sym being the external-symbols of :defdoc.layout-core
            collect sym)
      
      ;;; output-destinations.lisp
      #:output-destination
      #:output-filename
      #:open-file-args
      #:output-stream
      #:auxiliary-outputs
      
      #:fixed-layout
      #:fixed-width-layout
      #:fixed-height-layout
      #:flexible-layout
      #:no-layout
      #:page-width
      #:page-height
      #:page-size
      
      #:output-pages-are-files
      #:output-file-suffix
      #:get-next-filename
      #:get-next-output-stream
      #:with-next-output-stream
      
      ;;; output-formats.lisp
      #:pretty-printable-mixin
      #:print-pretty
      #:print-right-margin
      #:print-miser-width
      
      #:auxiliary-output
      #:main-output
      #:log-output
      #:html-output
      #:html-output-with-v3.2-styles
      #:html-output-with-embedded-css
      #:html-output-with-css
      #:single-page-html-output
      #:single-page-html-output-with-embedded-css
      #:multi-page-html-output
      #:multi-page-html-output-with-css
      #:css-output
      #:css-v2-output
      #:plain-text-output
      #:single-page-plain-text-output
      #:multi-page-plain-text-output
      #:multi-file-plain-text-output
      #:dvi-output
      #:ps-output
      #:latex-output
      
      ;;; engine-framework.lisp
      #:prepare
      #:append-to-vertical-sequence
      #:*header-level*
      #:compute-breaks
      #:active-break
      #:make-active-break
      #:active-break-breakpoint
      #:active-break-total-penalty
      #:active-break-fitting
      #:active-break-discretionary-p
      #:passive-break
      #:make-passive-break
      #:passive-break-breakpoint
      #:passive-break-previous
      #:with-breaking
      #:measure
      #:*word-count-cache*
      #:layout
      #:convert))
      
(defpackage :defdoc.layout.html-engine
  (:use :defdoc :defdoc.elements :defdoc.elements.section-header :defdoc.layout
        :common-lisp)
  (:shadow #:pprint-newline)
  (:export #:*suppress-html-tags*
           #:*html-head-elements*
           #:*suppress-html-wrapping*
           #:pprint-newline
           #:open-html-tag
           #:close-html-tag
           #:with-html-block
           #:write-html-tag
           #:write-html-entity
           #:write-html-char
           #:define-html-conversion))

(defpackage :defdoc.layout.flexible-layout
  (:use :defdoc :defdoc.elements :defdoc.elements.section-header :defdoc.layout
        :common-lisp))
