(in-package :defdoc.layout)

(defmethod prepare ((input section) (output no-layout))
  "Break sections into the header and each of the items in the contents."
  (append-to-vertical-sequence
   (list (make-instance 'section-header
           'header-level *header-level* 'contents (title input))))
  (let ((*header-level* (1+ *header-level*)))
    (mapc (lambda (element) (prepare element output)) (contents input))))

(defmethod compute-breaks (input elements (output no-layout))
  (list (make-instance (sub-container-class input) 'contents elements)))
