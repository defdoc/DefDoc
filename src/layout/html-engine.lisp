(in-package :defdoc.layout.html-engine)

;;;
;;; HTML output library
;;;

(defvar *suppress-html-tags* nil
  "Whether to suppress the output of HTML tags. Useful when converting document elements
that are defined by HTML to be un-marked-up strings. Does not suppress creation of
entities, however. Conversion routines might want to do something intelligent to make up
for the lack of markup if the markup is semantically significant.")

(defvar *html-head-elements* nil
  "The list of elements to be added to the HEAD when it is generated. For use in document
converters that need to add such elements (e.g. META, LINK, BASE, SCRIPT, STYLE), but
would still like to use CALL-NEXT-METHOD to avoid re-implementing the standard document
code.  The elements of this list are parameter lists for WRITE-HTML-TAG. Note that when
modifying this variable to include a BASE element, be sure to check for an existing one.")

(defvar *suppress-html-wrapping* nil
  "Whether to suppress the pretty-printer wrapping ability. Necessary for preformatted
text, e.g.")

(defvar *document-title*)
(defvar *document-subtitle*)

(defvar *html-stream*)

(defmacro pprint-newline (kind stream)
  `(unless *suppress-html-wrapping*
     (cl:pprint-newline ,kind ,stream)))

(declaim (inline %write-html-attributes))
(defun %write-html-attributes (&rest attributes)
  (loop
      for (key val . rest) on attributes by #'cddr
      do (when val
           (if (eq val t)               ;; just print the attribute with no value
               (write-string (string key) *html-stream*)
               (progn
                 (write-string (string key) *html-stream*)
                 (write-char #\= *html-stream*)
                 (typecase val
                   (integer (write val :stream *html-stream*))
                   (symbol (write-string (symbol-name val) *html-stream*))
                   (string (write-char #\" *html-stream*)
                           (write-string val *html-stream*)
                           (write-char #\" *html-stream*)))))
           (when rest
             (write-char #\space *html-stream*)
             (pprint-newline :fill *html-stream*)))))

(defun write-html-attributes (&rest attributes)
  (when attributes
    (write-char #\space *html-stream*)
    (pprint-indent :current 0 *html-stream*)
    (pprint-newline :miser *html-stream*)
    #+nil(apply #'%write-html-attributes
                (engine-specific-option element 'html-engine 'html-attributes))
    (apply #'%write-html-attributes attributes)))

(define-compiler-macro write-html-attributes (&whole whole &rest attributes)
  (when attributes
    (if (eq (car whole) 'write-html-attributes)
        `(progn
           (write-char #\space *html-stream*)
           (pprint-indent :current 0 *html-stream*)
           (pprint-newline :miser *html-stream*)
           #+nil(apply #'%write-html-attributes 
                (engine-specific-option element 'html-engine 'html-attributes))
           ,@(loop
                 with val-sym = (gensym "VAL-")
                 for (key val . rest) on attributes by #'cddr
                 collect `(let ((,val-sym ,val))
                            (when ,val-sym
                              (if (eq ,val-sym t) ;; just print the attribute with no value
                                  (write-string ,(string key) *html-stream*)
                                  (progn
                                    (write-string ,(string key)  *html-stream*)
                                    (write-char #\= *html-stream*)
                                    (typecase ,val-sym
                                      (integer (write ,val-sym :stream *html-stream*))
                                      (symbol (write-string (symbol-name ,val-sym)
                                                            *html-stream*))
                                      (string (write-char #\" *html-stream*)
                                              (write-string ,val-sym *html-stream*)
                                              (write-char #\" *html-stream*)))))
                              ,@(when rest '((write-char #\space *html-stream*)
                                             (pprint-newline :fill *html-stream*)))))))
        whole)))

(defmacro open-html-tag (tag-name &rest attributes)
  `(unless *suppress-html-tags*
     (pprint-logical-block (*html-stream* nil :prefix "<" :suffix ">")
       (write-string (string ,tag-name) *html-stream*)
       (write-html-attributes ,@attributes))))

(defmacro close-html-tag (tag-name)
  `(unless *suppress-html-tags*
     (write-string "</" *html-stream*)
     (write-string (string ,tag-name) *html-stream*)
     (write-char #\> *html-stream*)))

(defmacro with-html-block+ ((indent break-style tag &rest attributes) &body body)
  (let ((tag-sym (gensym "TAG-")))
    `(progn
       (pprint-newline :linear *html-stream*)
       (pprint-logical-block (*html-stream* nil)
         (let ((,tag-sym ,tag))
           (open-html-tag ,tag-sym ,@attributes)
           (pprint-indent :block ,indent *html-stream*)
           ,(and break-style `(pprint-newline ,break-style *html-stream*))
           (unwind-protect
               (progn ,@body)
             (pprint-indent :block 0 *html-stream*)
             ,(and break-style `(pprint-newline ,break-style *html-stream*))
             (close-html-tag ,tag-sym)))))))

(defmacro with-html-block ((tag &rest attributes) &body body)
  (let ((tag-sym (gensym "TAG-")))
    `(progn
       (pprint-newline :linear *html-stream*)
       (pprint-logical-block (*html-stream* nil)
         (let ((,tag-sym ,tag))
           (open-html-tag ,tag-sym ,@attributes)
           (pprint-indent :block 2 *html-stream*)
           (pprint-newline :linear *html-stream*)
           (unwind-protect
               (progn ,@body)
             (pprint-indent :block 0 *html-stream*)
             (pprint-newline :linear *html-stream*)
             (close-html-tag ,tag-sym)))))))

(defmacro with-html-inline ((tag &rest attributes) &body body)
  (let ((tag-sym (gensym "TAG-")))
    `(let ((,tag-sym ,tag))
       (open-html-tag ,tag-sym ,@attributes)
       (unwind-protect
           (progn ,@body)
         (close-html-tag ,tag-sym)))))

(defmacro write-html-tag (tag-name &rest attributes)
  "Writes a bodiless html tag. Uses the XHTML <.../> syntax."
  `(unless *suppress-html-tags*
     (pprint-logical-block (*html-stream* nil :prefix "<" :suffix "/>")
       (write-string ,(string tag-name) *html-stream*)
       (write-html-attributes ,@attributes))))

(defmacro write-html-tag* (tag-list)
  "Writes a bodiless html tag. Uses the XHTML <.../> syntax. The argument evaluates to an
arglist for WRITE-HTML-TAG."
  (let ((tag-sym (gensym "TAG-")))
    `(unless *suppress-html-tags*
       (let ((,tag-sym ,tag-list))
         (pprint-logical-block (*html-stream* nil :prefix "<" :suffix "/>")
           (write-string (string (first ,tag-sym)) *html-stream*)
           (apply #'write-html-attributes (rest ,tag-sym)))))))

(defmacro write-html-entity (form)
  (let ((val-sym (gensym "VAL-")))
    `(let ((,val-sym ,form))
       (write-char #\& *html-stream*)
       (write-string ,val-sym *html-stream*)
       (write-char #\; *html-stream*))))

(eval-when (:execute :compile-toplevel :load-toplevel)
  (unless (boundp '+escape-alist+)
    (defconstant +escape-alist+
        '((#\& . "amp")
          (#\< . "lt")
          (#\> . "gt")
          (#\" . "quot")))))

(defmacro write-html-char (form)
  (let ((val-sym (gensym "VAL-")))
    `(let ((,val-sym ,form))
       (cond
        ,@(mapcar (lambda (pair)
                    (destructuring-bind (char . escape) pair
                      `((eql ,val-sym ,char) (write-html-entity ,escape))))
                  +escape-alist+)
        (t (write-char ,val-sym *html-stream*))))))

;;;
;;; Abstraction of the way HTML will be generated
;;;

(defmacro define-html-conversion (type (element &optional (html-class 'html-output)) &body body)
  `(defmethod convert ((,element ,type) (.output. ,html-class))
     (flet ((convert (&optional (element (contents ,element)))
              (convert element .output.))
            (convert-to-text (element)
              (let ((*suppress-html-tags* t))
                (convert element .output.))))
       ,@body)))

;;;
;;; The actual converters
;;;

;;; The toplevel conversion routines, so we'll use a full defmethod and set up various
;;; dynamic variables to reasonable defaults

(defmethod convert ((input toplevel-element) (output html-output))
  (with-accessors ((title title)
                   (subtitle subtitle)
                   (author author)
                   (date date)
                   (copyright copyright)) input
    (flet ((convert-to-string (element)
             (with-output-to-string (*html-stream*)
               (let ((*suppress-html-tags* t))
                 (convert element output)))))
      (let ((*html-head-elements* *html-head-elements*)
            (*document-title* title)
            (*document-subtitle* subtitle))
        (when author
          (push (list :META :NAME "Author" :CONTENT (convert-to-string author))
                *html-head-elements*))
        (when date
          (push (list :META :NAME "Date" :CONTENT (convert-to-string date))
                *html-head-elements*))
        (when copyright
          (push (list :META :NAME "Copyright" :CONTENT (convert-to-string copyright))
                *html-head-elements*))
        #+nil (engine-specific-option input 'html-output 'head-elements)
        (call-next-method)))))

(defmethod convert ((input toplevel-subelement) (output html-output))
  (flet ((convert-to-text (element)
           (let ((*suppress-html-tags* t))
             (convert element output))))
    (with-next-output-stream (*html-stream* output)
      (with-html-block+ (0 :mandatory :HTML)
        (with-html-block+ (1 :mandatory :HEAD)
          (with-html-block (:TITLE)
            (convert-to-text *document-title*)
            (when *document-subtitle*
              (write-string " - " *html-stream*)
              (convert-to-text *document-subtitle*)))
          (dolist (element *html-head-elements*)
            (pprint-newline :linear *html-stream*)
            (write-html-tag* element)))
        (with-html-block+ (0 :mandatory :BODY)
          (convert (contents input) output))))))

;;; The simpler converters

(define-html-conversion section-header (input)
  (let* ((header-level (header-level input))
         (header-tag (case header-level
                       (1 :H1)
                       (2 :H2)
                       (3 :H3)
                       (4 :H4)
                       (5 :H5)
                       (6 :H6)
                       (t (error "Maximum section nesting level exceeded for HTML.")))))
    
    (when (< header-level 3) (pprint-newline :mandatory *html-stream*))
    (pprint-indent :block (1- (header-level input)) *html-stream*)
    (pprint-newline :mandatory *html-stream*)
    (with-html-block (header-tag)
      (convert))
    (pprint-indent :block (header-level input) *html-stream*)
    (pprint-newline :linear *html-stream*)))

(define-html-conversion paragraph (input)
  (with-html-block (:P)
    (convert)))

(define-html-conversion block-quotation (input)
  (with-html-block (:BLOCKQUOTE)
    (convert)))

(define-html-conversion itemized-list (input)
  (with-html-block (:UL)
    (dolist (element (contents input))
      (with-html-block (:LI)
        (convert element)))))

(define-html-conversion preformatted-paragraph (input)
  (with-html-block+ (0 nil :PRE)
    (let ((*suppress-html-wrapping* t))
      (convert))))

(define-html-conversion bold (input)
  (with-html-inline (:B)
    (convert)))

(define-html-conversion italic (input)
  (with-html-inline (:I)
    (convert)))

(define-html-conversion small-caps (input)
  (with-html-inline (:SPAN :style "font-variant: small-caps")
    (convert)))

(define-html-conversion link (input)
  (with-html-inline (:A :href (url input))
    (convert)))

(define-html-conversion discretionary-hyphen (input)
  (write-html-entity "shy"))

(define-html-conversion discretionary-break (input)
  (convert (no-break-elements input)))

(define-html-conversion character (input)
  (write-html-char input))

(define-html-conversion (eql #\space) (input)
  (write-html-char #\space)
  (pprint-newline :fill *html-stream*))

(define-html-conversion (eql #\newline) (input)
  (if *suppress-html-wrapping*
      (write-html-char #\newline)
      (progn
        (pprint-newline :miser *html-stream*)
        (write-html-tag :BR)
        (pprint-newline :linear *html-stream*))))

(define-html-conversion sequence (input)
  (map 'nil (lambda (item) (convert item))
       input))
