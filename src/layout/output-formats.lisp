(in-package :defdoc.layout)

;;;
;;; Format types for the output destination
;;;

(defclass pretty-printable-mixin ()
  ((print-pretty :type boolean
                 :initform t
                 :initarg print-pretty
                 :accessor print-pretty)
   (print-right-margin :type (integer 1)
                       :initform 72
                       :initarg print-right-margin
                       :accessor print-right-margin)
   (print-miser-width :type (or null (integer 0))
                      :initform 20
                      :initarg print-miser-width
                      :accessor print-miser-width)))

(defclass auxiliary-output (output-destination)
  ((main-output :initarg main-output
                :accessor main-output))
  (:documentation "A destination for auxiliary output of the layout engine."))

(defclass log-output (auxiliary-output)
  ()
  (:documentation "A destination for output about the operations of the layout engine."))

(defclass html-output (output-destination pretty-printable-mixin)
  ())

(defclass html-output-with-v3.2-styles (html-output)
  ())

(defclass html-output-with-embedded-css (html-output)
  ())

(defclass html-output-with-css (html-output)
  ())

(defclass single-page-html-output (html-output no-layout)
  ())

(defclass single-page-html-output-with-embedded-css
    (single-page-html-output html-output-with-embedded-css)
  ()
  (:documentation "A recommended mixture of the various html-output subclasses."))

(defclass multi-page-html-output (html-output flexible-layout output-pages-are-files)
  ((page-size :initform (make-spring 150 words + 50 words - 50 words))
   (output-file-suffix :initform ".html")))

(defclass multi-page-html-output-with-css (multi-page-html-output html-output-with-css)
  ()
  (:documentation "A recommended mixture of the various html-output subclasses."))

(defclass css-output (auxiliary-output pretty-printable-mixin)
  ((print-pretty :initform nil)))

(defclass css-v2-output (css-output)
  ())

(defclass plain-text-output (output-destination fixed-width-layout)
  ((page-width :initform (make-length 72 'em))))

(defclass single-page-plain-text-output (plain-text-output)
  ())

(defclass multi-page-plain-text-output (plain-text-output)
  ((page-height :initform (make-spring 25 em))))

(defclass multi-file-plain-text-output
    (multi-page-plain-text-output output-pages-are-files)
  ((page-height :initform (make-spring 60 em))
   (output-file-suffix :initform ".txt")))

(defclass dvi-output (output-destination fixed-layout)
  ())

(defclass pdf-output (output-destination fixed-layout)
  ())

(defclass ps-output (output-destination fixed-layout pretty-printable-mixin)
  ((print-pretty :initform nil)))

(defclass latex-output (output-destination no-layout pretty-printable-mixin)
  ())
