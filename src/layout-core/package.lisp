(defpackage :defdoc.layout-core
  (:use :defdoc :common-lisp)
  (:export
   ;;; aesthetics.lisp
   #:penalty
   #:penalty=
   #:penalty+
   #:penalty-incf
   #:penalty>
   #:penalty>=
   #:penalty<
   #:penalty<=
   #:penalty-min
   #:*discretionary-break-penalty*
   #:*discretionary-hyphen-penalty*
   #:*default-float-distance-penalty*
   #:*per-break-penalty*
   #:*adjacent-incompatible-fitting-penalty*
   #:*consecutive-discretionary-break-penalty*
   #:*break-across-discretionary-penalty*
   #:*final-break-discretionary-penalty*
   #:*lone-item-penalty*
   #:*lone-pair-penalty*
   #:*lone-heading-penalty*
   #:*pre-heading-penalty*
   #:*pre-list-penalty*
   #:*ideal-badness-tolerance*
   #:*acceptable-badness-tolerance*
   #:+infinitely-bad+
   #:badness-weighting
   #:*badness-weighting*
   #:layout-penalty
   #:compute-badness
   
   ;;; lengths.lisp
   #:length
   #:make-length
   #:length-amount
   #:length-units
   #:convert-length
   #:search-for-unit
   #:unit=
   
   #:meter #:meters #:m #:metre #:metres
   #:centimeter #:centimeters #:cm #:centimetre #:centimetres
   #:millimeter #:millimeters #:mm #:millimetre #:millimetres
   #:inch #:inches #:in
   #:point #:points #:pt
   #:scaled-point #:scaled-points #:sp
   #:pica #:picas #:pc
   #:em #:quad #:quads
   #:qquad #:qquads
   #:ex
   #:en
   #:degree #:degrees #:deg
   #:pixel #:pixels #:px
   #:word #:words))
