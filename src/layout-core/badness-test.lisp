(in-package :cl-user)
(import '(defdoc.layout-core::rational-badness
          defdoc.layout-core::fixnum-badness
          defdoc.layout-core::single-float-badness
          defdoc.layout-core::double-float-badness))

(defun null-badness (usage max)
  (declare (ignore usage max))
  0)

(defun time-badness ()
  "All timings seem to be essentially identical. Seems like loop overhead is greater than
computation of the badness."
  (declare (optimize (speed 3) (safety 1) (debug 0) (compilation-speed 0) (space 1)))
  #.`(let ((ext:*gc-verbose* nil))
       ,@(loop
             for function in
               '(null-badness rational-badness single-float-badness double-float-badness fixnum-badness)
             nconc `((ext:gc :full t)
                     (format t "~&Timing ~A:~%" ',function)
                     (time
                      (let* ((denom-max (expt 2 20))
                             (numer-max (truncate (log (* 8192 denom-max))))
                             (*random-state* (make-random-state))
                             dummy)
                        (dotimes (i 500)
                          (dotimes (j 500)
                            (setq dummy
                              (,function (/ (truncate (exp (random numer-max))) (1+ (random denom-max)))
                                         (/ (truncate (exp (random numer-max))) (1+ (random denom-max)))))))
                        (ext:gc :full t)
                        dummy))))))

(defun eval-badness-funcs (&optional (test-function 'fixnum-badness)
                                     (output *query-io*))
  (let* ((reference-function 'rational-badness)
         (denom-max (expt 2 20))
         (numer-max (truncate (log (* most-positive-fixnum denom-max))))
         (results '()))
    (dotimes (i 100)
      (dotimes (j 100)
        (let* ((denom (1+ (random denom-max)))
               (factor (rationalize (random 4.0)))
               (x (/ (truncate (exp (random numer-max))) denom))
               (y (* x factor))
               (reference-badness (funcall reference-function x y))
               (test-badness (funcall test-function x y))
               (difference (- test-badness reference-badness))
               (error (cond
                       ((= difference 0) 0)
                       ((zerop reference-badness) most-positive-short-float)
                       (t (float (/ (* 100 difference) reference-badness))))))
          (push (list x y reference-badness test-badness difference error) results))))
    (setf results (sort results '> :key (lambda (x) (abs (fifth x)))))
    (tagbody
     CONTINUE
      (format output "~&~10A | ~10A | ~10A | ~10A | ~10A | ~10A~%"
              "  USAGE" "    MAX" "    REF" "   TEST" "   DIFF" "  %ERROR")
      (when (interactive-stream-p output)
        (format output "~76,,,'-A~%" ""))
     MORE
      (dotimes (i 15)
        (let ((result (pop results)))
          (unless result (go EXIT))
          (apply (formatter "~&~10,4,2E | ~10,4,2E | ~10D | ~10D | ~10D | ~10,2F~%")
                 output result)))
      (if (interactive-stream-p output)
          (if (y-or-n-p "See more results? ")
              (go CONTINUE)
              (format t "~&~76,,,'.A~%" ""))
          (go MORE))
     EXIT
      (when (interactive-stream-p output)
        (format output "~&~76,,,'-A~%" "")))))

#|
CMUCL 3.1.0 (18d+) on Athlon XP 1700+:

real | user |  sys |  GC  | consing (MB)

Timing NULL-BADNESS:
0.69 | 0.54 | 0.02 | 0.13 | 13

Timing RATIONAL-BADNESS:
9.11 | 7.53 | 0.12 | 0.73 | 109

Timing SINGLE-FLOAT-BADNESS:
2.08 | 1.64 | 0.10 | 0.47 | 63

Timing DOUBLE-FLOAT-BADNESS:
3.21 | 2.49 | 0.14 | 0.71 | 106

Timing FIXNUM-BADNESS:
3.08 | 2.48 | 0.09 | 0.74 | 84

*-FLOAT-BADNESS give no error.

FIXNUM-BADNESS never gives errors over 5000 when USAGE and MAX are between 10^-6 and
10^6. Remaining errors over 100 and over 10% in that range are rare and scattered, with
FIXNUM-BADNESS returning infinitely-bad when it should return some value as low as
8000. Considering that such badnesses are rarely important to the final product, this
doesn't seem to be much of an issue.

CONCLUSION: *-FLOAT-BADNESS are the best, with FIXNUM-BADNESS as a fallback in case the
FPU is slow and/or inaccurate, with warning to choose units such that measurements stay
between 10^-5 and 10^5.

TODO: See if the accuracy of FIXNUM-BADNESS can be tweaked for larger values.
|#
