(in-package :defdoc.layout-core)

;;;
;;; Penalties for various layout decisions
;;;

(deftype penalty ()
  "Data type used to assess the aesthetic quality of a layout. The lower the penalty, the
nicer the layout supposedly looks. The goal of the layout engine is to find the layout
which minimizes the total penalty for the document. T indicates an infinite penalty; that
is, a layout option resulting in a penalty of T should never be taken. NIL indicates a
layout option that must always be taken. Note that the no-layout engine does not do any
layout, and so does not take any layout options. However, the conversion engine should
pass on the constraints to the target document if possible."
  '(or integer (member nil t)))

(declaim (inline two-arg-penalty+
                 penalty+ penalty=
                 two-arg-penalty>
                 penalty> penalty>= penalty< penalty<=
                 two-arg-penalty-min
                 penalty-min))

(defun gensym- (&rest args)
  (declare (ignore args))
  (gensym))

(defun two-arg-penalty+ (x y)
  (declare (optimize (speed 3) (safety 1) (space 0) (debug 0) (compilation-speed 0))
           (type penalty x y))
  (cond
   ((or (null y) (null x))
    (if (or (eq t x) (eq t y))
      (restart-case
          (error "Attempt to add an infinitely positive penalty to an infinitely negative penalty.")
        (use-0 () :report "Use 0 (zero) as the result." 0)
        (use-t () :report "Use T (+infinity) as the result." t)
        (use-nil () :report "Use NIL (-infinity) as the result." nil)
        (use-value (x) :report "Use some specific value as the result."
            :interactive (lambda () (format t "Enter a penalty: ") (eval (read)))
          x))
      nil))
   ((or (eq t x) (eq t y)) t)
   (t (locally (declare (type integer x y))
        (+ x y)))))

(defun penalty+ (&rest penalties)
  (declare (optimize (speed 3) (safety 1) (space 0) (debug 0) (compilation-speed 0)))
  (reduce #'two-arg-penalty+ penalties :initial-value 0))

(define-compiler-macro penalty+ (&whole whole &rest args)
  (if (and (eq 'penalty+ (first whole))
           (= (length whole) 3))
      `(two-arg-penalty+ ,(second whole) ,(third whole))
      whole))

(define-modify-macro penalty-incf (&rest penalties) penalty+)

(defun penalty= (&rest penalties)
  (declare (optimize (speed 3) (safety 1) (space 0) (debug 0) (compilation-speed 0)))
  (apply #'eql penalties))

(define-compiler-macro penalty= (&whole whole &rest args)
  (cond ((eq 'penalty= (first whole)) `(eql ,@(rest whole)))
        ((eq 'funcall (first whole)) `(eql ,@(rest (rest whole))))
        ((eq 'apply (first whole)) `(apply #'eql ,@(rest whole)))))

(defun two-arg-penalty> (x y)
  (declare (optimize (speed 3) (safety 1) (space 0) (debug 0) (compilation-speed 0))
           (type penalty x y))
  (cond
   ((eq t x) (not (eq t y)))
   ((null x) nil)
   ((eq t y) nil)
   ((null y) t) #| we already checked for a null x |#
   (t (> x y))))

(defun penalty> (&rest penalties)
  (declare (optimize (speed 3) (safety 1) (space 0) (debug 0) (compilation-speed 0)))
  (loop
      for x = (pop penalties) then y
      for y = (pop penalties) then (pop penalties)
      for result = (two-arg-penalty> x y)
      while result
      finally (return result)))

(define-compiler-macro penalty> (&whole whole &rest args)
  (if (and (eq (first whole) 'penalty>)
             (= (length whole) 3))
      `(two-arg-penalty> ,(second whole) ,(third whole))
      whole))

(defun penalty>= (&rest penalties)
  (declare (optimize (speed 3) (safety 1) (space 0) (debug 0) (compilation-speed 0)))
  (or (apply #'penalty= penalties) (apply #'penalty> penalties)))

(define-compiler-macro penalty>= (&whole whole &rest args)
  (if (eq (first whole) 'penalty>=)
      (let ((args (mapcar #'gensym- (rest whole))))
        `(let (,@(mapcar #'list args (rest whole)))
           (or (penalty= ,@args)
               (penalty> ,@args))))
      whole))

(defun penalty< (&rest penalties)
  (declare (optimize (speed 3) (safety 1) (space 0) (debug 0) (compilation-speed 0)))
  (not (or (apply #'penalty= penalties) (apply #'penalty> penalties))))

(define-compiler-macro penalty< (&whole whole &rest args)
  (if (eq (first whole) 'penalty<)
      (let ((args (mapcar #'gensym- (rest whole))))
        `(let (,@(mapcar #'list args (rest whole)))
           (not (or (penalty= ,@args)
                    (penalty> ,@args)))))
      whole))

(defun penalty<= (&rest penalties)
  (declare (optimize (speed 3) (safety 1) (space 0) (debug 0) (compilation-speed 0)))
  (or (apply #'penalty= penalties) (apply #'penalty< penalties)))

(define-compiler-macro penalty<= (&whole whole &rest args)
  (if (eq (first whole) 'penalty<=)
      (let ((args (mapcar #'gensym- (rest whole))))
        `(let (,@(mapcar #'list args (rest whole)))
           (or (penalty= ,@args)
               (penalty< ,@args))))
      whole))

(defun two-arg-penalty-min (x y)
  (declare (optimize (speed 3) (safety 1) (space 0) (debug 0) (compilation-speed 0))
           (type penalty x y))
  (if (two-arg-penalty> x y)
      y
      x))

(defun penalty-min (&rest penalties)
  (declare (optimize (speed 3) (safety 1) (space 0) (debug 0) (compilation-speed 0)))
  (reduce #'two-arg-penalty-min penalties :initial-value t))

(define-compiler-macro penalty-min (&whole whole &rest args)
  (if (and (eq (first whole) 'penalty-min)
           (= (length whole) 3))
      `(two-arg-penalty-min ,(second whole) ,(third whole))
      whole))

;;; Important note: TeX calls some of these demerits. It squares badness and penalties
;;; before adding in demerits, so the values here that correspond to TeX demerits should
;;; be square-rooted. XXX why does TeX do that?

(defvar *discretionary-break-penalty* 50
  "The default penalty for using a discretionary break.")

(defvar *discretionary-hyphen-penalty* 50
  "The penalty for using a discretionary hyphen break.")

(defvar *default-float-distance-penalty* 100
  "The default penalty for pushing a floating item into an overflow container.")

(defvar *per-break-penalty* 10
  "The penalty assessed each time a break is added. Keeps the layout engines from breaking
excessively.")

(defvar *adjacent-incompatible-fitting-penalty* 100
  "The penalty assessed when adjacent breaks cause fittings that are vastly different.")

(defvar *consecutive-discretionary-break-penalty* 100
  "The penalty for having two consecutive breaks use discretionary breaks. Called
double_hyphen_demerits in TeX.")

(defvar *break-across-discretionary-penalty* 50
  "The penalty for breaking on a discretionary break and then breaking at a higher level
across that same break.")

(defvar *final-break-discretionary-penalty* 70
  "The penalty for using a discretionary break as the final one in a container. Called
final_hyphen_demerits in TeX.")

(defvar *lone-item-penalty* 150
  "The penalty for breaking in order to separate an item from the rest of its
siblings. Assessed once for each end of the container, so a two-item container would have
this penalty assessed twice on the break between those items.")

(defvar *lone-pair-penalty* 50
  "The penalty for breaking in order to separate a pair of items from the rest of their
siblings. Assessed once for each end of the container, so a four-item container would have
this penalty assessed twice on the central break and a three-item container would have the
two breaks assessed both this and *LONE-ITEM-PENALTY*.")

(defvar *lone-heading-penalty* 10000
  "The penalty for breaking in order to separate a heading from the first element of its
section. Default is not T because sometimes breaking there helps the layout in a
last-ditch effort.")

(defvar *pre-heading-penalty* -50
  "The penalty for breaking before the heading of a section.")

(defvar *pre-list-penalty* 20
  "The penalty for breaking before a listed-information container.")

(defvar *ideal-badness-tolerance* 100
  "The tolerance for badness per break on the first pass, before using discretionary breaks.")

(defvar *acceptable-badness-tolerance* 200
  "The tolerance for badness per break on the second pass, when considering discretionary breaks.")

;;;
;;; Badness of container packing
;;;

(deftype badness-weighting ()
  "A data structure to indicate how to compute a penalty from a badness. It is a cons with
the car being a factor and the cdr being a description of how that factor is to be
applied: If it is a UNIT name, then the car is multiplied by the badness ratio and then by
the total springiness in the direction used.  If it is NIL, then the car is multiplied
just by the badness ratio."
  '(cons (rational 0) symbol))

(defvar *badness-weighting* (cons 10000 nil))

#+nil
(defun layout-penalty (badness-weighting actual natural stretch shrink units)
  "Given a badness-weighting, the actual length of the container, the natural length,
stretch, and shrink of the container, all measured in the same given units, this function
computes the penalty to be accrued for this layout. The units of the weighting and the
units of the container measurements must be of the same type (both textual or both visual,
etc.)."
  (declare (type rational actual natural)
           (type (rational 0) stretch shrink)
           (type badness-weighting badness-weighting)
           (optimize (speed 3) (safety 1) (debug 0)))
  (let ((weighting-factor (car badness-weighting))
        (weighting-units (cdr badness-weighting)))
    (multiple-value-bind (badness direction) (calculate-badness actual natural stretch shrink)
      (nth-value 1
                 (round
                  (case weighting-units
                    ((nil) (* weighting-factor badness))
                    (t (ecase direction
                         (0 0)
                         (1 (* weighting-factor badness
                               (convert-length-or-lose (make-length stretch units)
                                                       weighting-units)))
                         (-1 (* weighting-factor badness
                                (convert-length-or-lose (make-length shrink units)
                                                        weighting-units)))))))))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defconstant +infinitely-bad+ 10000))

(declaim (inline rational-badness fixnum-badness single-float-badness double-float-badness))

(defun rational-badness (spring-usage spring-max)
  "A heuristic to calculate the malaesthetics caused by the stretching or shrinking of
springiness in a container. This is the ideal algorthm."
  (declare (type (rational 0) spring-usage spring-max)
           (optimize (speed 3) (safety 1) (debug 0) (compilation-speed 0) (space 1)))
  (cond
   ((= spring-usage 0) 0)
   ((= spring-max 0) +infinitely-bad+)
   (t (let ((ratio (/ spring-usage spring-max)))
        (if (>= ratio (rationalize (expt (/ +infinitely-bad+ 100) 1/3)))
            +infinitely-bad+
            (nth-value 0 (truncate (* 100 (expt ratio 3)))))))))

(defun single-float-badness (spring-usage spring-max)
  (declare (type (rational 0) spring-usage spring-max)
           (optimize (speed 3) (safety 1) (debug 0) (compilation-speed 0) (space 1)))
  (let ((spring-usage (float spring-usage 0.0s0))
        (spring-max (float spring-max 0.0s0)))
  (cond
   ((= spring-usage 0) 0)
   ((= spring-max 0) +infinitely-bad+)
   (t (let ((ratio (/ spring-usage spring-max)))
        (if (>= ratio (float (expt (/ +infinitely-bad+ 100) 1/3) 0.0s0))
            +infinitely-bad+
            (locally
                (declare (type (single-float 0.0s0 #.(float (expt (/ +infinitely-bad+ 100) 1/3) 0.0s0))
                               ratio))
              (nth-value 0 (truncate (* 100 (expt ratio 3)))))))))))

(defun double-float-badness (spring-usage spring-max)
  (declare (type (rational 0) spring-usage spring-max)
           (optimize (speed 3) (safety 1) (debug 0) (compilation-speed 0) (space 1)))
  (let ((spring-usage (float spring-usage 0.0d0))
        (spring-max (float spring-max 0.0d0)))
  (cond
   ((= spring-usage 0) 0)
   ((= spring-max 0) +infinitely-bad+)
   (t (let ((ratio (/ spring-usage spring-max)))
        (if (>= ratio (float (expt (/ +infinitely-bad+ 100) 1/3) 0.0d0))
            +infinitely-bad+
            (locally
                (declare (type (double-float 0.0d0 #.(float (expt (/ +infinitely-bad+ 100) 1/3) 0.0d0))
                               ratio))
              (nth-value 0 (truncate (* 100 (expt ratio 3)))))))))))

(defun fixnum-badness (spring-usage spring-max)
  (declare (optimize (speed 3) (safety 1) (debug 0) (compilation-speed 0) (space 1))
           (type (rational 0) spring-usage spring-max))
  (let* ((accuracy-bits 16)
         (accuracy (expt 2 accuracy-bits))
         (infinitely-bad                ; badness for values that would cause an overflow
                                        ; during calculation
          #+nil (ceiling (* 1.1 (/ most-positive-fixnum accuracy)))
          +infinitely-bad+)
         (factor                        ; this ends up being the factor of 100 after cubing
          (round (expt (* 100 accuracy) 1/3)))
         (limit                         ; values larger than this won't benefit from the
                                        ; optimized algorithm since their scaled
                                        ; representations will overflow fixnum range
          (floor most-positive-fixnum accuracy))
         (ratio-limit                   ; don't cube if it'll end up overflowing fixnum range
          (floor (expt most-positive-fixnum 1/3)))
         (usage-scaled-limit            ; make sure that multiplication does not overflow
          (truncate most-positive-fixnum factor))
         (max-scaled-limit              ; make sure that we don't waste time diving when
                                        ; it'll be infinitely-bad anyway
          (ceiling usage-scaled-limit (/ ratio-limit factor))))
    (cond
     ((<= spring-usage (/ accuracy)) 0)
     ((<= spring-max (/ accuracy)) infinitely-bad)
     ((or (>= spring-max limit) (>= spring-usage limit))
      (let ((ratio (/ spring-usage spring-max)))
        (if (>= ratio (rationalize (expt (/ +infinitely-bad+ 100) 1/3)))
            +infinitely-bad+
            (nth-value 0 (truncate (* 100 (expt ratio 3)))))))
     (t (locally (declare (type (rational 0 #.(ceiling most-positive-fixnum (expt 2 16)))
                                        ; change this when above definitions change!!
                                spring-usage spring-max))
          (let* ((spring-usage-scaled (truncate spring-usage (/ accuracy)))
                 (spring-max-scaled (truncate spring-max (/ accuracy)))
                 (ratio (cond
                         ((<= spring-usage-scaled usage-scaled-limit)
                          (ceiling (* spring-usage-scaled factor) spring-max-scaled))
                         ((>= spring-max-scaled max-scaled-limit)
                          (ceiling spring-usage-scaled (ceiling spring-max-scaled factor)))
                         (t (return-from fixnum-badness infinitely-bad)))))
            (if (> ratio ratio-limit)
                infinitely-bad
                (nth-value 0 (truncate (expt ratio 3) accuracy)))))))))

(eval-when (:compile-toplevel :execute)
  (defconstant +badness-function+ 'single-float-badness))

(defun compute-badness (actual natural stretch shrink)
  "Computes the badness of putting a container with the given natural size, shrink, and
stretch in the given actual length. All values should have the same units. The second
return value indicates whether the container is being stretched, shrunk, or is at its
natural size."
  (declare (type rational actual natural)
           (type (rational 0) stretch shrink)
           (optimize (speed 3) (safety 1) (debug 0) (compilation-speed 0) (space 1)))
  (cond
   ((= actual natural) (values 0 0))
   ((> actual natural) (values (#.+badness-function+ (- actual natural) stretch) 1))
   (t                  (values (#.+badness-function+ (- natural actual) shrink) -1))))
