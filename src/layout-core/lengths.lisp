(in-package :defdoc.layout-core)

(declaim (optimize (speed 3) (safety 1) (debug 0)))

;;;
;;; External interface
;;;

(deftype length ()
  "Data type used to specify lengths and distances."
  '(cons rational symbol))

(declaim (inline make-length length-amount length-units))

(defun make-length (amount units)
  (let ((units (or (and (eq (symbol-package units) #.*package*)
                        units)
                   (find-symbol (symbol-name units) #.*package*)
                   (error "Unit name ~A not found in ~A package."
                          units (package-name #.*package*)))))
  (cons amount units)))

(defun length-amount (length)
  (car length))

(defun length-units (length)
  (cdr length))

(defun convert-length (length units)
  "Converts the length to be expressed in the given units. If this is not possible or
requires information about, e.g., the target medium, NIL will be returned."
  (multiple-value-bind (from-units from-type) (search-for-unit (length-units length))
    (let ((to-units (find-unit from-type units)))
      (if to-units
          (convert-units from-type (length-amount length) from-units to-units)))))

(defun convert-length-or-lose (length units)
  "Converts the length to be expressed in the given units. If this is not possible or
requires information about, e.g., the target medium, an error will be signaled."
  (multiple-value-bind (from-units from-type) (search-for-unit (length-units length))
    (let ((to-units (find-unit from-type units)))
      (if to-units
          (convert-units from-type (length-amount length) from-units to-units)
          (error "Units in conversion from ~A ~A to ~A do not match."
                 (length-amount units)
                 (if (= 1 (length-amount units)) (unit-name from-units) (plural from-units))
                 (plural (search-for-unit to-units)))))))

(defun unit= (x y)
  (or (eq x y)
      (multiple-value-bind (x-object unit-type) (search-for-unit x)
        (and x-object (eq (cannonical-name x-object)
                          (cannonical-name (find-unit unit-type y)))))))

;;;
;;; Internal framework
;;;

(defclass unit ()
  ((unit-name :type symbol
              :initarg :unit-name
              :accessor unit-name)
   (unit-type :type symbol
              :initarg :unit-type
              :accessor unit-type)
   (plural :type symbol
           :initform nil
           :initarg :plural
           :accessor plural)
   (abbreviation :type symbol
                 :initform nil
                 :initarg :abbreviation
                 :accessor abbreviation)
   (cannonical-name :type symbol
                    :initarg :cannonical-name
                    :accessor cannonical-name)
   (definition :type (cons rational symbol)
     :initarg :definition
     :accessor definition)))

(defmethod cannonical-name ((object (eql t)))
  t)

(defmethod cannonical-name ((object null))
  nil)

(declaim (type list *units*))
(defvar *units* '())
(eval-when (:load-toplevel :execute)
  (dolist (type '(:absolute :font-relative :visual :textual))
    (pushnew (cons type '()) *units*
             :key 'car)))

(defun units-of-type (type)
  (declare (type symbol type))
  (let ((units (assoc type *units*)))
    (if units
        (the list (cdr units))
        (error "Unit type ~A not found." type))))

(defsetf units-of-type (type) (store)
  (let ((units (gensym "UNITS-")))
    `(let ((,units (assoc ,type *units*)))
       (setf (cdr ,units) ,store))))

(defun search-for-unit (unit)
  (dolist (unit-type *units*)
    (let ((unit (find-unit (car unit-type) unit)))
      (when unit (return (values unit (car unit-type)))))))

(defun find-unit (type unit)
  (declare (type symbol type unit))
  (find unit (units-of-type type) :key 'unit-name :test 'eq))

(defun convert-units (type value from-unit to-unit)
  "from-unit and to-unit must be unit objects"
  (declare (rational value)
           (symbol type from-unit to-unit))
  (let ((from-parent (cdr (definition from-unit)))
        (to-parent (cdr (definition to-unit))))
    (cond
     ((eq from-parent (unit-name to-unit))
      (* value (the rational (car (definition from-unit)))))
     ((eq to-parent (unit-name from-unit))
      (/ value (the rational (car (definition to-unit)))))
     (t (or (and from-parent
                 (convert-units type
                                (convert-units type value from-unit from-parent)
                                from-parent
                                to-unit))
                                        ; XXX not sure if I technically need to recurse on both
            (and to-parent
                 (convert-units type
                                (convert-units type value from-unit to-parent)
                                to-parent
                                to-unit)))))))

(defmacro define-length-unit (name type &key plural abbreviation variants definition)
  (let ((definition (and definition (cons (first definition) (second definition))))
        (name (intern (symbol-name name) #.*package*)))
    `(progn
       (ensure-length-unit :unit-name ',name
                           :cannonical-name ',name
                           :unit-type ',type
                           :plural ',plural
                           :abbreviation ',abbreviation
                           :definition ',definition)
       (ensure-length-unit :unit-name ',plural
                           :cannonical-name ',name
                           :unit-type ',type
                           :definition ',definition)
       (ensure-length-unit :unit-name ',abbreviation
                           :cannonical-name ',name
                           :unit-type ',type
                           :definition ',definition)
       ,@(mapcar (lambda (variant)
                   `(ensure-length-unit :unit-name ',variant
                                        :cannonical-name ',name
                                        :unit-type ',type
                                        :definition ',definition))
                 variants))))

(defun ensure-length-unit (&rest initargs &key unit-name unit-type &allow-other-keys)
  (let ((found-unit (find-unit unit-type unit-name)))
    (if found-unit
        (apply #'reinitialize-instance found-unit initargs)
        (push (apply #'make-instance 'unit initargs)
              (units-of-type unit-type)))))

;;;
;;; Unit definitions
;;;

(define-length-unit meter :absolute
  :plural meters
  :abbreviation m
  :variants (metre metres))

(define-length-unit centimeter :absolute
  :plural centimeters
  :abbreviation cm
  :variants (centimetre centimetres)
  :definition (1/100 m))

(define-length-unit millimeter :absolute
  :plural millimeters
  :abbreviation mm
  :variants (millimetre millimetres)
  :definition (1/1000 m))

(define-length-unit inch :absolute
  :plural inches
  :abbreviation in
  :definition (254/100 cm))

(define-length-unit point :absolute
  :plural points
  :abbreviation pt
  :definition (100/7272 in))

(define-length-unit scaled-point :absolute
  :plural scaled-points
  :abbreviation sp
  :definition (1/65536 pt))

(define-length-unit pica :absolute
  :plural picas
  :abbreviation pc
  :definition (12 pt))

(define-length-unit em :font-relative
  :variants (quad quads))

(define-length-unit qquad :font-relative
  :plural qquads
  :definition (2 em))

(define-length-unit ex :font-relative)

(define-length-unit en :font-relative
  :definition (1/2 em))

(define-length-unit degree :visual
  :plural degrees
  :abbreviation deg)

(define-length-unit pixel :visual
  :plural pixels
  :abbreviation px
  :definition (227/10000 deg)           ;; from the CSS2 spec
                                        ;; 0.28mm at "arm's length" (28 inches)
                                        ;; for paper (read at 21 inches), 0.21mm
                                        ;; 0.01 mm per inch of viewing length
  )

(define-length-unit word :textual
  :plural words)
