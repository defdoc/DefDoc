(defpackage :DefDoc
  (:documentation
   "The DefDoc package, a document processing system inspired by TeX, HTML, XML, and CSS.")
  (:use :common-lisp))
