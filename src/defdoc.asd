(asdf:defsystem :DefDoc
    :description "An extensible, object-oriented, dynamic document creation system."
    :long-description "DefDoc lets you create structured documents using
a hierarchial format where each of the elements is an object. The
document can be modified interactively and dynamically and can be output
to any format for which an output engine is defined."
    :properties ((:debian-package-name . "defdoc"))
    :maintainer "Rahul Jain <rjain@common-lisp.net>"
    :licence "MIT"
    :version "0.0.1"
    
    :components
  ((:module :core
            :pathname ""
            :components ((:file "package")))
   (:module :layout-core
            :components
            ((:file "package")
             (:file "lengths"
                    :depends-on ("package"))
             (:file "aesthetics"
                    :depends-on ("package")))
            :depends-on (:core))
   (:module :elements
            :components
            ((:file "package")
             (:file "color"
                    :depends-on ("package"))
             #+nil (:file "fonts"
                          :depends-on ("package"))
             (:file "basic-elements"
                    :depends-on ("package"))
             (:file "toplevel"
                    :depends-on ("basic-elements"))
             (:file "structural"
                    :depends-on ("basic-elements"))
             (:file "textual"
                    :depends-on ("basic-elements"))
             (:file "style"
                    :depends-on ("textual"))
             (:file "abbreviations"
                    :depends-on ("basic-elements"))
             (:file "links"
                    :depends-on ("textual")))
            :depends-on (:layout-core))
   #+nil
   (:module :font-metrics
            :components
            ((:file "package")
             (:file "tfm"
                    :depends-on ("package"))
             (:file "tfm-reader"
                    :depends-on ("tfm"))
             (:file "afm"
                    :depends-on ("package"))
             (:file "afm-reader"
                    :depends-on ("afm"))
             (:file "freetype")
             :depends-on ("package"))
            :depends-on (:core))
   (:module :layout
            :components
            ((:file "package")
             (:file "output-destinations"
                    :depends-on ("package"))
             (:file "output-formats"
                    :depends-on ("output-destinations"))
             (:file "engine-framework"
                    :depends-on ("output-formats"))
             (:file "no-layout-engine"
                    :depends-on ("engine-framework" "output-formats"))
             (:file "flexible-layout-engine"
                    :depends-on ("engine-framework" "output-formats"))
             (:file "html-engine"
                    :depends-on ("output-formats" "no-layout-engine")))
            :depends-on (:layout-core :elements))
   (:file "user"
          :depends-on (:layout :elements))
   (:module "frontends"
            :components
            ((:file "package")
             (:module "basic"
                      :components
                      ((:file "package")
                       (:file "engine"
                              :depends-on ("package")))))
            :depends-on (:layout :elements)))
  :depends-on (:specified-types))
