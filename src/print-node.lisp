#|
(put 'define-print-node 'fi:common-lisp-indent-hook 1)
|#

(in-package :lambdatex)

(defvar *font-in-short-display* 0
  "The font code that is assumed to be present when SHORT-DISPLAY begins;
deviations from this font will be printed.")

(defun short-display (list &optional (stream *standard-output*))
  "Prints highlights of list P.

Adapted from the TeX sources, sections 174, 175, and 267."
  (declare (type list list))
  (let ((*standard-output* stream))
    (dolist (n list)
      (typecase n
	(char-node
	 (when (not (eq (char-node-font n) *font-in-short-display*))
	   ;; print font identifier (section 267)
	   ;; Knuth checks that the font id is between font_base and
	   ;; font_max. If we use symbols, this is not needed.
	   (princ (char-node-font n))
	   (princ #\space)
	   (setq *font-in-short-display* (char-node-font n)))
	 (princ (char-node-char n)))
	(rule-node (princ #\|))
	(glue-node (when (not (eq (glue-node-spec n) +zero-glue+))
		     (princ #\space)))
	(math-node (princ #\$))
	(ligature-node (short-display (ligature-node-char-list n)))
	(disc-node (short-display (disc-node-pre-break n))
		   (short-display (disc-node-post-break n))
		   (dotimes (i (disc-node-replace-count n))
		     (when n (setq n (disc-node-link n)))))
	((or box-node mark-node adjust-node unset-node whatsit-node)
	 (princ "[]"))))))

;;; SHOW-NODE-LIST helpers from TeX sources, section 176
;;; basically, the TeX pretty-printer.
;;; TODO: need to check if nesting/indentation works.
;;; maybe make these use FORMAT?

(defmacro define-print-node (type &body body)
  `(defmethod print-object ((n ,type) stream)
     (if *print-readably*
	 (call-next-method)
	 (let ((*standard-output* stream))
	   ,@body))))

(define-print-node char-node
  ;;; print font identifier (section 267).
  ;;; Again, Knuth checks for a valid id.
  (princ (char-node-font n))
  (princ #\space)
  (princ (char-node-char n)))

(defun print-mark (n)
  (declare (type mark-node n))
  (princ #\{)
  (show-token-list (mark-node-link n)
		   nil
		   (- +max-print-line+ 10)) ;; hrm??
  (princ #\}))

(defun print-rule-dimen (n)
  (if (null n) (princ #\*) (princ n)))

(defun print-glue (dim order s)
  "Prints glue stretch and shrink, possibly followed by the name of finite
units.

Adapted from TeX sources, section 177."
  (princ dim)
  (when (not (eq order :normal)) (princ (symbol-name order)))
  (when (not (null s)) (princ s)))

(defun print-glue-spec (n s)
  "Prints a whole glue specification.

Adapted from TeX sources, section 178."
  (if (null n)
      (princ #\*)
      (progn
	(princ (glue-spec-width n))
	(when (not (null s)) (princ s))
	(when (/= 0 (glue-spec-stretch n))
	  (princ " plus ")
	  (print-glue (glue-spec-stretch n) (glue-spec-stretch-order n) s))
	(when (/= 0 (glue-spec-shrink n))
	  (print-glue (glue-spec-shrink n) (glue-spec-shrink-order n) s)))))

;;; From section 691, need procedures for displaying elements of mlists
;;; From section 225, need PRINT-SKIP-PARAM

;;; SHOW-NODE-LIST is basically the lisp printer.

(define-print-node hlist-node
  (princ ":h")
  (call-next-method))

(define-print-node vlist-node
  (princ ":v")
  (call-next-method))

(define-print-node box-node
  (princ "box(")
  (princ (box-node-height n))
  (princ #\+)
  (princ (box-node-depth n))
  (princ ")x")
  (princ (box-node-width n))
  (let ((glue (box-node-glue-set n))
	(sign (box-node-glue-sign n)))
    (when (or (/= g 0) (not (eq sign :normal)))
      (princ ", glue set ")
      (when (eq sign :shrinking)
	(princ "- "))
      (cond
       ((> glue 20000) (princ #\>) (setq glue 20000))
       ((< glue -20000) (princ "< -") (setq glue 20000)))
      (print-glue glue (box-node-glue-order n) 0)))
  ;; now recurr on the contents
  (write (box-node-list n) :stream stream))

(define-print-node unset-node
  (princ ":unsetbox(")
  (princ (unset-node-height n))
  (princ #\+)
  (princ (unset-node-depth n))
  (princ ")x")
  (princ (unset-node-width n))
  (when (> (unset-node-span-count n) 0)
    (princ " (")
    (princ (unset-node-span-count n))
    (princ " columns"))
  (when (/= 0 (unset-node-stretch 0))
    (princ ", stretch ")
    (print-glue (unset-node-glue-stretch n)
		(unset-node-glue-stretch-order n)
		nil))
  (when (/= 0 (unset-node-shrink 0))
    (princ ", shrink ")
    (print-glue (unset-node-glue-shrink n)
		(unset-node-glue-shrink-order n)
		nil))
  ;;; now recurr on the contents
  (write (unset-node-list n) :stream stream))

(define-print-node rule-node
  (princ ":rule(")
  (print-rule-dimen (rule-node-height n))
  (princ #\+)
  (print-rule-dimen (rule-node-depth n))
  (princ ")x")
  (print-rule-dimen (rule-node-width n)))

(define-print-node insertion-node
  (princ ":insert")
  (princ (insertion-node-number n))
  (princ ", natural size")
  (princ (insertion-node-height n))
  (princ "\; split(")
  (print-glue-spec (insertion-node-split-top n) nil)
  (princ #\,)
  (print (insertion-node-depth n))
  (princ "\; float cost ")
  (princ (insertion-node-float-cost n))
  ;;; now recurr on the contents
  (write (insertion-node-vlist n) :stream stream))

(define-print-node glue-node
  (let ((type (glue-node-type n)))
    (princ ":glue")
    (when (not (eq :normal type))
      (princ #\()
      (princ (case type
	       (:cond-math ":nonscript")
	       (:mu ":mscript")
	       (t (skip-param type))))
      (princ #\)))
    (when (not (eq :cond-math type))
      (princ #\space)
      (print-glue (glue-node-spec n)
		  (if (eq :mu type) "mu" nil)))))

(define-print-node leader-node
  (princ #:)
  (case (leader-node-type n)
    (:centered (princ #\c))
    (:expanded (princ #\x)))
  (princ "leaders")
  (print-glue-spec (leader-node-spec n) nil)
  ;; now recurr on the contents
  (write (leader-node-leader n) :stream stream))

(define-print-node kern-node
  (if (not (eq :mu (kern-node-type n)))
      (progn
	(princ ":kern")
	(when (not (eq :normal (kern-node-type n)))
	  (princ #\space))
	(princ (kern-node-width n))
	(when (eq :accent (kern-node-type n))
	  (princ " (for accent)")))
      (progn
	(princ ":mkern")
	(princ (kern-node-width n))
	(princ "mu"))))

(define-print-node math-node
  (princ ":math")
  (if (eq :before (math-node-position n))
      (princ "on")
      (princ "off"))
  (when (/= 0 (math-node-width n))
    (princ " surrounded ")
    (princ (math-node-width n))))

(define-print-node ligature-node
  (call-next-method)
  (princ " (ligature ")
  (when (member (ligature-node-boundaries n) '(:left :both))
    (princ "|"))
  (let ((*font-in-short-display* (ligature-node-font n)))
    (short-display (ligature-node-char-list n)))
  (when (member (ligature-node-boundaries n) '(:right :both))
    (princ "|"))		
  (princ #\)))

(define-print-node penalty-node
  (princ ":penalty ")
  (princ (penalty-node-penalty n)))

(define-print-node disc-node
  (princ ":discretionary")
  (when (> (disc-node-replace-count n) 0)
    (princ " replacing ")
    (princ (disc-node-replace-count n)))
  ;; recur on the pre-break
  (write (disc-node-pre-break n) :stream stream)
  (princ #\|)				; this should actually add a "|" to
					; the end of the line leader.
  ;; recur on the pre-break
  (write (disc-node-post-break n) :stream stream))

(define-print-node mark-node
  (princ ":mark")
  (print-mark n))

(define-print-node adjust-node
  (princ ":vadjust")
  ;; recurr on contents
  (write (adjust-node-vlist n) :stream stream))
