(in-package :defdoc.frontends.basic)

(defvar *documents* (make-hash-table :test 'eq))

(defun quote-keynames (arglist)
  (loop
      for (name val) on arglist by #'cddr
      collect `',name
      collect val))

(defun make-contents-form (contents)
  (if (every (lambda (item) (typep item '(or string character))) contents)
      (apply 'concatenate 'string contents)
      `(list ,@(mapcar (lambda (item)
                         (if (or (stringp item) (characterp item))
                             item
                             `(doc ,@item)))
                       contents))))

(defmacro doc (type &optional initargs &body body)
  (let ((initargs (quote-keynames initargs)))
    `(make-instance ',type
       'contents ,(make-contents-form body)
       ,@initargs)))

(defmacro defdoc (name (&optional (type 'document) &rest initargs) &body body)
  (let ((initargs (quote-keynames initargs)))
    `(setf (gethash ',name *documents*)
           (make-instance ',type
             'contents ,(make-contents-form body)
             ,@initargs))))

(defun undefdoc (name)
  (remhash name *documents*))

(defun find-doc (name)
  (gethash name *documents*))

(defun convert-doc (name target-type &rest other-initargs)
  (convert (find-doc name)
           (apply #'make-instance target-type other-initargs)))

(defmacro pprint-doc-body ()
  '(progn
    (pprint-indent :block 1)
    (loop
      (pprint-exit-if-list-exhausted)
      (write-char #\space)
      (pprint-newline :linear)
      (pprint-doc-element *standard-output* (pprint-pop)))))

(defmacro pprint-doc-initargs ()
  '(progn
    (pprint-exit-if-list-exhausted)
    (loop
      (write (pprint-pop))              ; key
      (write-char #\space)
      (pprint-newline :miser)
      (write (pprint-pop))              ; val
      (pprint-exit-if-list-exhausted)
      (write-char #\space)
      (pprint-newline :fill))))

(set-pprint-dispatch
 '(cons (member defdoc))
 (lambda (*standard-output* obj)
   (pprint-logical-block (*standard-output* obj :prefix "(" :suffix ")")
     (write (pprint-pop))               ; defdoc
     (write-char #\space)
     (pprint-newline :miser)
     (pprint-indent :current 0)
     (write (pprint-pop))               ; name
     (write-char #\space)
     (pprint-newline :fill)
     (pprint-logical-block (*standard-output*
                            (pprint-pop) :prefix "(" :suffix ")")
       (pprint-exit-if-list-exhausted)
       (write (pprint-pop))             ; type
       (write-char #\space)
       (pprint-newline :linear)
       (pprint-indent :current 0)
       (pprint-doc-initargs))
     (pprint-doc-body))))

(defun pprint-doc-element (*standard-output* obj)
  (pprint-logical-block (*standard-output* obj :prefix "(" :suffix ")")
    (pprint-indent :block 3)
    (write (pprint-pop))                ; type
    (write-char #\space)
    (pprint-newline :fill)
    (pprint-logical-block (*standard-output* (pprint-pop) :prefix "(" :suffix ")")
      (pprint-doc-initargs))
    (pprint-indent :block 1)
    (pprint-doc-body)))

(set-pprint-dispatch
 '(cons (member doc))
 (lambda (*standard-output* obj)
   (pprint-logical-block (*standard-output*
                          obj :prefix "(" :suffix ")")
     (write (pprint-pop))               ; doc
     (write-char #\space)
     (pprint-newline :miser)
     (pprint-indent :current 0)
     (write (pprint-pop))               ; type
     (write-char #\space)
     (pprint-newline :fill)
     (pprint-logical-block (*standard-output* (pprint-pop) :prefix "(" :suffix ")")
       (pprint-doc-initargs))
     (pprint-indent :block 1)
     (pprint-doc-body))))
