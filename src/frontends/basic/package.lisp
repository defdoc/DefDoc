(defpackage :defdoc.frontends.basic
  (:use :defdoc :defdoc.layout :defdoc.elements :common-lisp)
  (:export
   #:defdoc #:undefdoc #:find-doc #:convert-doc #:doc))

(defpackage :defdoc.frontends.basic-user
  (:use :defdoc :defdoc.elements :defdoc.layout :common-lisp :defdoc.frontends.basic))
