;;;;
;;;; Here's the actual reader
;;;; From the TeX sources, sections 560ff.
;;;;

(eval-when (load eval)
  (handler-bind
      (logical-pathname-translations "tex-font-metrics")
    ((file-error
      #'(lambda (condition)
          (declare (ignore condition))
          (setf (logical-pathname-translations "tex-font-metrics")
                `(("**;*.*.*" "/usr/share/texmf/fonts/tfm/**/")
                  ("**;*" "/usr/share/texmf/fonts/tfm/**/*.tfm")
                  ("**;*" "**/*.tfm")
                  ("**;*.*.*" "**/")
                  ("*.*.*" "/usr/share/texmf/fonts/tfm/public/cm/")
                  ("*" "/usr/share/texmf/fonts/tfm/public/cm/*.tfm"))))))))

(define-condition font-error (error)
  ((userid :reader font-error-userid :initarg :userid :type string)
   (filename :reader font-error-filename :initarg :name :type string))
  (:report #'(lambda (condition stream)
	       (format stream
		       "Font metrics for font ~A in file ~A are invalid."
		       (font-error-userid condition)
		       (font-error-filename condition)))))

(defun read-font-info (userid name &key (area t) (size nil))
  "Reads the specified TFM file and returns the font-info structure
corresponding to that file.
USERID is the user-specified TeX identifier for the font.
NAME is a logical-pathname representing the name of the TFM file,
        or a physical pathname if AREA is nil.
AREA is a logical-pathname with wildcards which will be merged with NAME or
        T if the defaults should be used (host tex-fonts, type tfm)
        or NIL if no pathname merging should be done.
SIZE is the point size at which the font should be loaded, if positive,
        the negative of the scale factor to be used, if negative,
        or NIL if the ``natural'' size of the font should be used.

Adapted from TeX sources, sections 560ff."
  (declare (type string userid name)
	   (type (or boolean string pathname) area)
	   (type (or nil rational) size))
  (let ((file
	 (cond
	  ((eq t area) (merge-pathnames name
					(make-pathname :host "tex-fonts"
						       :type "tfm")))
	  ((null area) name)
	  (t (merge-pathnames name area)))))
    (with-open-file (input file
		     :direction :input :element-type 'unsigned-byte)
      (labels
	  ((abort () (error 'font-error :userid userid :filename name))
	   (read-8 () (the (unsigned-byte 8)
			(or (read-byte input nil nil)
			    (error))))
	   (read-16 () (the (unsigned-byte 16)
			 (let ((b (read-8)))
			   (if (> b 127)
			       (abort)
			       (+ (* b #o400) (read-8))))))
	   (read-32 () (the (unsigned-byte 32)
			 (let ((b0 (read-8))
			       (b1 (read-8))
			       (b2 (read-8))
			       (b3 (read-8)))
			   (values (+ (* b0 (expt 2 24))
				      (* b1 (expt 2 16))
				      (* b2 (expt 2 8))
				      b3)
				   b0 b1 b2 b3))))
	   (check-existence (c)
	     (check-char-range c)
	     (when (not (check-exists info c)) (abort))))
	(declare (inline read-8 read-16 read-32))
	;;; read the TFM size fields
	(let* (z
	       (lf (read-16))
	       (lh (read-16))
	       (bc (read-16))
	       (ec (read-16))
	       (dummy (when (or (> bc (+ ec 1)) (> ec 255)) (abort)))
	       (dummy (when (> bc 255) (setq bc 1
					     ec 0)))
	       (nw (read-16))
	       (nh (read-16))
	       (nd (read-16))
	       (ni (read-16))
	       (nl (read-16))
	       (nk (read-16))
	       (ne (read-16))
	       (np (read-16))
	       (dummy
		(when (/= lf (+ 6 lh (- ec bc -1) nw nh nd ni nl nk ne np))
		  (abort)))
	       ((info (create-font-info bc ec nw nh nd ni nl nk ne np))))
	  
	  ;;; read the TFM header
	  (when (< lh 2) (abort))
	  (setf (font-info-check info) (read-32))
	  (setq z (read-16))
	  (setq z (+ (* z #o400) (read-8)))
	  (setq z (+ (* z #o20) (floor (read-8) #o20)))
	  (when (< z (expt 2 16)) (abort))
	  (dotimes (i (* 4 (- lh 2)))
	    ;;; ignore the rest
	    (read-8))
	  (setf (font-design-size info) (setq z (/ z (expt 2 16))))
	  (setf (font-size info)
		(cond ((not (and size (/= size -1)))
		       (font-design-size info))
		      ((>= size 0) size)
		      (t (/ z (- s))))))
	(flet ((check-char-range (c) (when (not (< bc c ec)) (abort))))
	  (declare (inline check-char-range))
					;;; read character data
	  (dotimes (k (- ec bc -1))
	    (multiple-value-bind (t a b c d) (read-32)
	      (setf (aref (font-info-chars info) k) t)
	      (when (or (>= a nw)
			(>= (floor b #o20) nh)
			(>= (floor b #o20) nd)
			(>= (floor c 4) ni))
		(abort))
	      (case (floor c 4)
		(+lig-tag+ (when (>= d nl) (abort)))
		(+ext-tag+ (when (>= d ne) (abort)))
		(+list-tag+
		 ;;; check that there is no cycle of characters linked
		 ;;; together by list-tag entries
		 (check-char-range d)
		 (loop
		     with d = d then (char-info-word-rem-byte qw)
		     while (> d (+ k bc))
		     do (when (= d (+ k bc)) (abort))
		     with qw = (char-info info d)
		     while (= (char-info-word-tag qw) +list-tag+))))))
	  ;;; read box dimensions
	  (flet ((read-scaled () (the fix-word (floor (* z (read-32))))))
	    (declare (inline read-scaled))
	    (dotimes (k nw)
	      (setf (aref (font-info-widths font) k) (read-scaled)))
	    (when (/= 0 (aref (font-info-widths font 0))) (abort))
	    (dotimes (k nh)
	      (setf (aref (font-info-heights font) k) (read-scaled)))
	    (when (/= 0 (aref (font-info-heights font 0))) (abort))
	    (dotimes (k nd)
	      (setf (aref (font-info-depths font) k) (read-scaled)))
	    (when (/= 0 (aref (font-info-depths font 0))) (abort))
	    (dotimes (k ni)
	      (setf (aref (font-info-italics font) k) (read-scaled)))
	    (when (/= 0 (aref (font-info-italics font 0))) (abort)))
	  ;;; read ligature/kern programs
	  (let ((bch-label #o77777)
		(bchar 256))
	    (when (> nl 0)
	      (dotimes (k (- nl +kern-base-offset+))
		(multiple-value-bind (v a b c d) (read-32)
		  (setf (aref (font-info-lig-kern-progs info) k) v)
		  (if (> a 128)
		      (progn
			(when (>= (+ (* 256 c) d) nl)
			  (abort))
			(when (and (= a 255) (= k 0))
			  (setf bchar b)))
		      (progn
			(when (/= b bchar) (check-existence b))
			(cond
			 ((< c 128) (check-existence d))
			 ((>= (+ (* 256 (- c 128)) d) nk) (abort)))
			(when (and (< a 128) (>= (+ k a 1) nl))
			  (abort))))
		  (when (= a 255) (setf bch-label (+ (* 256 c) d)))))
	      (dotimes (k nk)
		(setf (aref (font-info-kern-progs info) k)
		      (read-scaled))))))
        ;;; Read extensible char recipes
	(dotimes (k ne)
	  (multiple-value-bind (v a b c d) (read-32)
	    (setf (aref (font-info-extensible-recipes info) k) v)
	    (when (/= a 0) (check-existence a))
	    (when (/= b 0) (check-existence b))
	    (when (/= c 0) (check-existence c))
	    (check-existence d)))
	;;; Read font parameters
	(setf (font-info-params info)
	      (make-fix-word-vector (max np 7)))
	(setf (aref (font-info-params info) 0)
              (let ((x (read-32)))
                (floor (if (> x (- (expt 2 31) 1))
                           (- x (expt 2 32))
                           x)
                       #o20)))))))
