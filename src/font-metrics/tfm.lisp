;(in-package :lambdatex:font-info)

(declaim (optimize (speed 3) (safety 1) (debug 1)))

;;;
;;; The definition of the TFM format begins in section 539 of the TeX
;;; sources.
;;;

(deftype fix-word-vector () '(or (simple-array (signed-byte 32) (*)) nil))
(defmacro make-fix-word-vector (size &rest rest)
  `(make-array ,size
	       :element-type '(signed-byte 32)
	       :initial-element 0
	       ,@rest))
(declaim (inline fix-word-rational))
(defun fix-word-rational (word)
  (declare (type (signed-byte 32) word))
  (/ word (expt 2 20)))

(deftype char-info-word () '(unsigned-byte 32))
(declaim (inline char-info-word-width-index
		 char-info-word-height-index
		 char-info-word-depth-index
		 char-info-word-italic-index
		 char-info-word-tag
		 char-info-word-rem-byte))
(defun char-info-word-width-index (word)
  (declare (type char-info-word word))
  (ldb (byte 8 24) word))
(defun char-info-word-height-index (word)
  (declare (type char-info-word word))
  (ldb (byte 4 20) word))
(defun char-info-word-depth-index (word)
  (declare (type char-info-word word))
  (ldb (byte 4 16) word))
(defun char-info-word-italic-index (word)
  (declare (type char-info-word word))
  (ldb (byte 6 10) word))
(defun char-info-word-tag (word)
  (declare (type char-info-word word))
  (ldb (byte 2 8) word))
(defun char-info-word-rem-byte (word)
  (declare (type char-info-word word))
  (ldb (byte 8 0) word))

;;; Constants for the tag field of a char-info-word, indicating how to
;;; interpret the remainder field.
;;; Adapted from TeX sources, section 544.
(defconstant +no-tag+ 0)
(defconstant +lig-tag+ 1)
(defconstant +list-tag+ 2)
(defconstant +ext-tag+ 3)

;;; Constants for the lig-kern array.
;;; Adapted from TeX sources, section 545.
(defconstant +stop-flag+ 128)
(defconstant +kern-flag+ 128)

(deftype kern-command () '(unsigned-byte 32))
(declaim (inline kern-command-skip-byte
		 kern-command-next-char
		 kern-command-op-btye
		 kern-command-rem-byte))
(defun kern-command-skip-byte (word)
  (declare (type kern-command word))
  (ldb (byte 8 24) word))
(defun kern-command-next-char (word)
  (declare (type kern-command word))
  (ldb (byte 8 16) word))
(defun kern-command-op-byte (word)
  (declare (type kern-command word))
  (ldb (byte 8 8) word))
(defun kern-command-rem-byte (word)
  (declare (type kern-command word))
  (ldb (byte 8 0) word))

(deftype extensible-recipe () '(unsigned-byte 32))
(declaim (inline ext-top ext-mid ext-bot ext-rep))
(defun ext-top (word)
  (declare (type extensible-recipe word))
  (ldb (byte 8 24) word))
(defun ext-mid (word)
  (declare (type extensible-recipe word))
  (ldb (byte 8 16) word))
(defun ext-bot (word)
  (declare (type extensible-recipe word))
  (ldb (byte 8 8) word))
(defun ext-rep (word)
  (declare (type extensible-recipe word))
  (ldb (byte 8 0) word))

(defconstant +slant-code+ 0)
(defconstant +space-code+ 1)
(defconstant +space-stretch-code+ 2)
(defconstant +space-shrink-code+ 3)
(defconstant +x-height-code+ 4)
(defconstant +quad-code+ 5)
(defconstant +extra-space-code+ 6)

(eval-when (compile load eval)
(defstruct font-info
  "Structure containing the information pertaining to a specific font.

Adapted from TeX sources, sections 549 and 550."
  (check 0 :type unsigned-byte)
  (size 0 :type rational)
  (design-size 0 :type rational)
  (num-params 0 :type unsigned-byte)
  ;(name "" :type base-string)
  ;(area "" :type base-string)
  (beginning-code 0 :type (unsigned-byte 8))
  (ending-code 255 :type (unsigned-byte 8))
  (glue nil :type (or glue-spec nil))
  (usedp nil :type boolean)
  (hyphen-char #\- :type (or base-char nil))
  (skew-char nil :type (or base-char nil))
  (boundary-char-start nil :type (or fixnum nil))
  (boundary-char nil :type (or nil (unsigned-byte 8)))
  (false-boundary-char-p nil :type boolean)
  ;;; now comes the actual data
  (chars nil :type (or nil (simple-array char-info-word (*))))
  (widths nil :type fix-word-vector)
  (heights nil :type fix-word-vector)
  (depths nil :type fix-word-vector)
  (italics nil :type fix-word-vector)
  (lig-kern-progs nil :type (or (simple-array kern-command (*)) nil))
  (kern-progs nil :type (or (simple-array kern-command (*)) nil))
  (extensible-recipies nil
		       :type (or (simple-array extensible-recipe (*))
				 nil))
  (params nil :type fix-word-vector))
)

(defun create-font-info (bc ec nw nh nd ni nl nk ne np)
  (make-font-info :beginning-code bc
		  :ending-code ec
		  :chars (make-array (- ec bc -1)
				     :element-type char-info-word)
		  :widths (make-fix-word-vector nw)
		  :heights (make-fix-word-vector nh)
		  :depths (make-fix-word-vector nd)
		  :italics (make-fix-word-vector ni)
		  :lig-kern-progs (make-fix-word-vector
				   (+ nl +kern-base-offset+))
		  :kern-progs (make-fix-word-vector
			       (- nk +kern-base-offset+))
		  :extensible-recipes (make-fix-word-vector ne)
		  :params (make-fix-word-vector (max np 7))))

(defconstant +null-font+
    ;;; needs to be interned in some package or hashtable where the rest of
    ;;; the fonts will go, too
    (make-font-info :name "nullfont"
		    :beginning-code 1
		    :ending-code 0
		    :num-params 7
		    :params (make-fix-word-vector 7))
  "The null font. Adapted from section 552 of the TeX sources.")

(declaim (inline char-info char-width char-exists char-italic
		 char-height char-depth char-tag))
(defun char-info (f c)
  (declare (type font-info f)
	   (type (unsigned-byte 8) c))
  (aref (font-info-chars f) c))
(defun char-width (f c)
  (declare (type font-info f)
	   (type (unsigned-byte 8) c))
  (aref (font-info-widths f) (char-info-word-width-index (char-info f c))))
(defun char-exists (f c)
  (declare (type font-info f)
	   (type (unsigned-byte 8) c))
  (> (aref (font-info-widths f)
	   (char-info-word-width-index (char-info f c)))
     0))
(defun char-italic (f c)
  (declare (type font-info f)
	   (type (unsigned-byte 8) c))
  (aref (font-info-italics f)
	(char-info-word-italic-index (char-info f c))))
(defun char-height (f c)
  (declare (type font-info f)
	   (type (unsigned-byte 8) c))
  (aref (font-info-heights f)
	(char-info-word-height-index (char-info f c))))
(defun char-depth (f c)
  (declare (type font-info f)
	   (type (unsigned-byte 8) c))
  (aref (font-info-depths f) (char-info-word-depth-index (char-info f c))))
(defun char-tag (f c)
  (declare (type font-info f)
	   (type (unsigned-byte 8) c))
  (char-info-word-tag (char-info f c)))

(defconstant +null-character+ 0)

(declaim (inline char-kern lig-kern-start lig-kern-restart))
(defun char-kern (f j)
  (declare (type font-info f)
	   (type kern-command j))
  (aref (font-info-kern-progs f) (+ (* 256 (kern-command-op-byte j))
				    (kern-command-rem-byte j))))
(defun lig-kern-start (f j)
  (declare (type font-info f)
	   (type char-info-word j))
  (aref (font-info-lig-kern-progs f) (char-info-word-rem-byte j)))
(defconstant +kern-base-offset+ (* 256 128))
(defun lig-kern-restart (f i)
  (declare (type font-info f)
	   (type kern-command i))
  (aref (font-info-lig-kern-progs f) (+ (* 256 (kern-command-op-byte i))
					(kern-command-rem-byte i)
					32768
					(- +kern-base-offset+))))

(declaim (inline font-info-slant
		 font-info-space
		 font-info-space-stretch
		 font-info-space-shrink
		 font-info-x-height
		 font-info-quad
		 font-info-extra-space))
(defun font-info-slant (f)
  (declare (type font-info f))
  (aref (font-info-params f) +slant-code+))
(defun font-info-space (f)
  (declare (type font-info f))
  (aref (font-info-params f) +space-code+))
(defun font-info-space-stretch (f)
  (declare (type font-info f))
  (aref (font-info-params f) +space-stretch-code+))
(defun font-info-space-shrink (f)
  (declare (type font-info f))
  (aref (font-info-params f) +space-shrink-code+))
(defun font-info-x-height (f)
  (declare (type font-info f))
  (aref (font-info-params f) +x-height-code+))
(defun font-info-quad (f)
  (declare (type font-info f))
  (aref (font-info-params f) +quad-code+))
(defun font-info-extra-space (f)
  (declare (type font-info f))
  (aref (font-info-params f) +extra-space-code+))


