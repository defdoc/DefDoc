(asdf:operate 'asdf:load-op :defdoc.contrib.project-website)

(defpackage :defdoc.website
    (:use :defdoc :defdoc.elements :defdoc.layout :common-lisp :defdoc.frontends.basic
          :defdoc.contrib.project-website))

(in-package :defdoc.website)

(defabbreviation DefDoc (doc italic () "Def") (doc small-caps () "Doc"))
(defabbreviation LaTeX "LaTeX")

(defun gen-src-link (target)
  (concatenate 'string "/cgi-bin/viewcvs.cgi/DefDoc/src" target "?cvsroot=defdoc"
               (unless (eql (aref target (1- (length target))) #\/)
                 "&rev=HEAD&content-type=text/vnd.viewcvs-markup")))

(defun gen-mailing-list-link (target)
  (concatenate 'string "/mailman/listinfo/defdoc-" target))

(defdoc index (project-website name (doc DefDoc)
                               short-description "An extensible, dynamic document creation system"
                               author "Rahul Jain")
  (documentation ()
                 (paragraph ()
                            "I have written up a document that is an overview of the goals and rationale behind "(DefDoc)". "
                            "It is available as "
                            (link (url "overview.tex") (LaTeX))", "
                            (link (url "overview.pdf") "PDF")", and "
                            (link (url "overview.ps") "Postscript")"."))
  (code ()
        (paragraph ()
                   "The code is very incomplete, but what there is is "
                   (link (url (gen-src-link "/")) "publically available")". "
                   "There are definitions for various types of "
                   (link (url (gen-src-link "/elements/")) "document elements")". "
                   "There are also definitions for the abstract types of "
                   (link (url (gen-src-link "/layout/")) "layout engines")
                   " as well as the "
                   (link (url (gen-src-link "/layout/html-engine.lisp")) " HTML layout engine")". "
                   "Finally, there is a "
                   (link (url (gen-src-link "/frontends/basic/")) "basic frontend")", "
                   "essentially a few simple macros for creating "(DefDoc)" documents. "
                   "A more fully-featured frontend is planned, which will be sensitive to the "
                   "current context and allow texual content to be entered unquoted.")
        (paragraph ()
                   "This site is actually a "(DefDoc)" "
                   (link (url (gen-src-link "/../website/website.lisp")) "document")"."))
  (dependencies ()
                (link (url "http://www.common-lisp.net/projects/rjain-utils/specified-types.html")
                      "Specified types"))
  (communication ()
                 (paragraph ()
                            "There are 3 mailing lists for "(DefDoc)", "
                            (link (url (gen-mailing-list-link "announce")) "defdoc-announce")", "
                            (link (url (gen-mailing-list-link "devel")) "defdoc-devel")", and "
                            (link (url (gen-mailing-list-link "cvs")) "defdoc-cvs")".")))
