
;;; Example usage of the DefDoc macro (tested in cmucl, sbcl, and lispworks):

(in-package :defdoc.frontends.basic-user)

(defdoc test (document title (doc bold () "Title (html-output should strip bold)")
                       author "Rahul Jain")
  (paragraph () "This is a test...")
  (section (title "The test")
           (paragraph () "Section text <= All text")
           (section (title "The plot thickens")
                    (paragraph () "This is a long paragraph. "
                               (bold () "It has various styles ")
                               (italic () "in it, too" (bold () "!"))
                               " This is more text to extend the paragraph.")
                    (section (title "Deeper in the jungle")
                             (preformatted-paragraph ()
                                                     "This is some preformatted text with a line break!
 And a little indentation.")
                             (section (title (doc italic () "Italic section title"))
                                      (paragraph () "\"Stuff\" with a line
break.")
                                      (paragraph () "More stuff, &c.")))))
  (section (title "Conclusion")
           (block-quotation ()
                            "That's one small step for a man; one giant leap for mankind. "
                            (italic () "-- Neil Armstrong"))))

(convert-doc 'test 'single-page-html-output 'output-filename "/tmp/out.html" 'print-pretty t)

(convert-doc 'test 'multi-page-html-output 'output-filename "/tmp/out"
             'page-size (make-spring 15 words + 5 words - 5 words))
