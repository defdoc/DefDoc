(defpackage :specified-types
  (:use :common-lisp)
  (:export
   #:unassociated-specifier
   #:define-specified-type
   #:define-specified-type-converter))

(in-package :specified-types)

(defun make-weak-hash-table ()
  (let ((table (make-hash-table :test 'eq
                                #+cmu :weak-p #+cmu t
                                #+allegro :weak-keys #+allegro t)))
    #+lispworks (hcl:set-hash-table-weak table t)
    table))

(define-condition unbinder-without-namespace (style-warning)
  ((type-name :initarg :type-name
              :reader type-name))
  (:report (lambda (condition stream)
             (format stream "In DEFINE-SPECIFIED-TYPE ~A, unbinder argument given when no ~
namespace requsted."
                     (type-name condition)))))

(define-condition definer-without-namespace (style-warning)
  ((type-name :initarg :type-name
              :reader type-name))
  (:report (lambda (condition stream)
             (format stream "In DEFINE-SPECIFIED-TYPE ~A, definer argument given when no ~
namespace requsted."
                     (type-name condition)))))

(define-condition unassociated-specifier (cell-error)
  ()
  (:report (lambda (condition stream)
             (let ((name (cell-error-name condition)))
               (format stream "As a ~A specifier, ~A has no associated value."
                       (first name) (second name)))))
  (:documentation "Errors resulting from attempts to access the value associated with some
specifier when there is no associated value. The name of the cell (accessed with
CELL-ERROR-NAME) is bound to a list of the specified type and the specifier object."))

(defun read-new-value ()
  (format t "Enter a new value: ")
  (multiple-value-list (eval (read))))

(defmacro define-specified-type (name &key (cannonical-types (list name))
                                           (predicate t)
                                           namespacep
                                           (unbinder namespacep)
                                           (bound-predicate namespacep)
                                           definer)
  (let ((hyphenated-name-p (find #\- (symbol-name name)))
        (table (gensym (concatenate 'string
                         "*" (symbol-name name) "-TABLE*-")))
        (doc-table (gensym (concatenate 'string
                             "*" (symbol-name name) "-DOC-TABLE*-"))))
    (when (not namespacep)
      (when unbinder
        (warn 'unbinder-without-namespace :type-name name))
      (when definer
        (warn 'definer-without-namespace :type-name name)))
    `(progn
       (defgeneric ,name (object)
         (:documentation ,(format nil "Converts specifiers for the type ~A to objects of ~
the type ~A."
                                  name name)))
       (defmethod ,name :around ((object t))
         (restart-case
             (call-next-method)
           (bind-name (value)
               :report "Attempt to bind the name to some value and use that value."
               :interactive read-new-value
             (setf (,name object) value))
           (use-value (value)
               :report "Specify a value to be used instead."
               :interactive read-new-value
             value)))
       (defmethod ,name ((object t))
         (error 'unassociated-specifier :name (list ',name object)))
       ,@(mapcar (lambda (type) `(defmethod ,name ((object ,type)) object))
                 cannonical-types)
       ,@(when predicate
           (when (eq predicate t)
             (setf predicate
                   (intern (concatenate 'string (symbol-name name)
                                        (if hyphenated-name-p "-P" "P"))
                           (symbol-package name))))
           `((defgeneric ,predicate (object)
               (:documentation ,(format nil "Determines whether the given object is of the type ~A."
                                        name)))
             (defmethod ,predicate ((object t)) nil)
             ,@(mapcar (lambda (type) `(defmethod ,predicate ((object ,type)) t))
                       cannonical-types)))
       ,@(when namespacep
           (when (not definer)
             (setf definer (intern (concatenate 'string
                                     (if hyphenated-name-p "DEFINE-" "DEF")
                                     (symbol-name name))
                                   (symbol-package name))))
           `((defvar ,table (make-weak-hash-table))
             (defvar ,doc-table (make-weak-hash-table))
             (defmethod ,name ((object symbol))
               (multiple-value-bind (value foundp) (gethash object ,table)
                 (if foundp
                     value
                     (error 'unassociated-specifier :name (list ',name object)))))
             (defmethod (setf ,name) (new-value (object symbol))
               (check-type new-value (or ,@cannonical-types))
               (setf (gethash object ,table) new-value))
             ,(when unbinder
                (when (eq unbinder t)
                  (setf unbinder
                        (intern (concatenate 'string (symbol-name name)
                                             "-MAKUNBOUND")
                                (symbol-package name))))
                `(defmethod ,unbinder ((object symbol))
                   (remhash object ,table)
                   (remhash object ,doc-table)))
             ,(when bound-predicate
                (when (eq bound-predicate t)
                  (setf bound-predicate
                        (intern (concatenate 'string (symbol-name name)
                                             "-BOUNDP")
                                (symbol-package name))))
                `(defmethod ,bound-predicate ((object symbol))
                   (nth-value 1 (gethash object ,table))))
             (defmethod documentation ((object symbol) (doc-type (eql ',name)))
               (gethash object ,doc-table))
             (defmethod (setf documentation)
                 (new-value (object symbol) (doc-type (eql ',name)))
               (if (nth-value 1 (gethash object ,table))
                   (setf (gethash object ,doc-table) new-value)
                   (warn 'unassociated-specifier :name (list ',name object))))
             (defmacro ,definer (name constructor &optional docstring)
               `(progn
                  (setf (,',name ',name) ,constructor)
                  ,(when docstring `(setf (documentation ',name ',',name) ,docstring))
                  ',name))))
       ',name)))

(defmacro define-specified-type-converter (to from &body body)
  "Defines a method for using objects of the type FROM as specifiers for objects of the
type TO. Binds the specifier object to the name of the parameter FROM in the body."
  `(defmethod ,to ((,from ,from))
     ,@body))
